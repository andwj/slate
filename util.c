// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

#include "local.h"

#ifdef _WIN32
#define PATH_SEPARATOR  '\\'
#define PATH_SEP_STR    "\\"
#else
#define PATH_SEPARATOR  '/'
#define PATH_SEP_STR    "/"
#endif

#ifndef _WIN32
#include <sys/stat.h>
#include <sys/types.h>
#endif

void * UT_Alloc(size_t size) {
	void * p = malloc(size);
	if (p == NULL) {
		Die("out of memory\n");
	}
	return p;
}

int UT_MakeDir(const char *name) {
#ifdef _WIN32
	return (::CreateDirectory(name, NULL) != 0);

#else // UNIX or MACOSX

	return (mkdir(name, 0777) == 0);
#endif
}

int UT_Compare(const char *A, const char *B) {
	for (;;) {
		int AC = tolower((unsigned char) *A);
		int BC = tolower((unsigned char) *B);

		if (AC != BC) { return AC - BC; }
		if (AC ==  0) { return 0;       }

		A++; B++;
	}
}

char * UT_Strdup(const char *s) {
	size_t slen   = strlen(s);
	char * result = UT_Alloc(slen + 1);
	memcpy(result, s, slen + 1);
	return result;
}

int UT_HasBOM(const char *s) {
	uint8_t *p = (uint8_t *)s;

	return (p[0] == 0xEF) && (p[1] == 0xBB) && (p[2] == 0xBF);
}

char * UT_JoinPath(const char *dir, const char *rest) {
	size_t len1   = strlen(dir);
	size_t len2   = strlen(rest);
	char * result = UT_Alloc(len1 + len2 + 2);

	memcpy(result, dir, len1);
	result[len1] = PATH_SEPARATOR;
	memcpy(result + len1 + 1, rest, len2 + 1);

	return result;
}

int UT_MatchArg(const char *arg, char shortname, const char *longname) {
	// a short option can only have a single `-` dash
	if (arg[1] != 0 && arg[2] == 0) {
		if (arg[1] == shortname) {
			return 1;  // yes!
		}
	}

	// a long option may have either one or two `-` dashes
	arg++;
	if (*arg == '-') {
		arg++;
	}
	if (longname != NULL) {
		if (strcmp(arg, longname) == 0) {
			return 1;  // yes!
		}
	}

	return 0;  // no
}
