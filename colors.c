// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

#include "local.h"

uint8_t gamma[256];
uint8_t palette[256][3];

void C_InitGamma(void) {
	if (conf.gamma < 1) {
		conf.gamma = 1;
	}

	double factor = 100.0 / (double)conf.gamma;

	int g;
	for (g = 0 ; g < 256 ; g++) {
		double ity = g / 255.0;
		int ity2 = 255.0 * pow(ity, factor);
		gamma[g] = (ity2 < 0) ? 0 : (ity2 > 255) ? 255 : ity2;
	}
}

static void C_SetColor(int c, uint8_t r, uint8_t g, uint8_t b) {
	palette[c][0] = r;
	palette[c][1] = g;
	palette[c][2] = b;
}

static int C_Sixth(int n) {
	// follow the xterm colors, n = 0..5
	return 55 + n * 40;
}

void C_InitColors(void) {
	int c;

	C_SetColor( 0,   0,   0,   0);
	C_SetColor( 1, 170,   0,   0);
	C_SetColor( 2,   0, 170,   0);
	C_SetColor( 3, 170, 128,   0);
	C_SetColor( 4,   0,   0, 170);
	C_SetColor( 5, 170,   0, 170);
	C_SetColor( 6,   0, 170, 170);
	C_SetColor( 7, 170, 170, 170);

	C_SetColor( 8,  85,  85,  85);
	C_SetColor( 9, 255,   0,   0);
	C_SetColor(10,   0, 255,   0);
	C_SetColor(11, 255, 255,   0);
	C_SetColor(12,   0,   0, 255);
	C_SetColor(13, 255,   0, 255);
	C_SetColor(14,   0, 255, 255);
	C_SetColor(15, 255, 255, 255);

	// the 6x6x6 color cube
	for (c = 0 ; c < 216 ; c++) {
		uint8_t r = C_Sixth(c / 36);
		uint8_t g = C_Sixth((c / 6) % 6);
		uint8_t b = C_Sixth(c % 6);

		C_SetColor(16 + c, r, g, b);
	}

	// grayscale ramp, excluding full black and full white
	for (c = 0 ; c < 24 ; c++) {
		uint8_t ity = 255 * (c + 1) / 25;

		C_SetColor(232 + c, ity, ity, ity);
	}
}

void C_GetColor(uint8_t col, SDL_Color *out) {
	out->r = gamma[palette[col][0]];
	out->g = gamma[palette[col][1]];
	out->b = gamma[palette[col][2]];
	out->a = 255;
}

void C_Contrast(SDL_Color *bg, SDL_Color *fg) {
	if (bg->r == fg->r && bg->g == fg->g && bg->b == fg->b) {
		// keep the background the same

		fg->r += (fg->r > 192) ? -32 : 32;
		fg->g += (fg->g > 192) ? -32 : 32;
		fg->b += (fg->b > 192) ? -32 : 32;
	}
}

uint8_t C_DimColor(uint8_t c) {
	// we assume a dark-mode terminal, where dim = closer to black.
	// on a white background, probably want: dim = closer to white.

	// for greyscale, look in the ramp
	if (palette[c][0] == palette[c][1] &&
		palette[c][0] == palette[c][2]) {

		uint8_t ity = palette[c][1];

		if (ity < 15) {
			return 0;
		}

		return 232 + ity / 18;
	}

	// for normal range, remap to the color cube
	if (c <= 7) {
		uint8_t r = (c & 1) ? 2 : 0;
		uint8_t g = (c & 2) ? 2 : 0;
		uint8_t b = (c & 4) ? 2 : 0;

		return 16 + (r * 36) + (g * 6) + b;
	}

	// for bright range, just produce their normal counterpart
	if (c <= 15) {
		return c - 8;
	}

	// in the color cube, reduce each dimension
	c -= 16;

	uint8_t r = c / 36;
	uint8_t g = (c / 6) % 6;
	uint8_t b = c % 6;

	if (r + g + b <= 1) {
		// too dark, use a grey instead
		return 232 + 5;
	}

	r = r * 3 / 5;
	g = g * 3 / 5;
	b = b * 3 / 5;

	return 16 + (r * 36) + (g * 6) + b;
}

uint8_t C_OppositeColor(int c) {
	if (c < 16) {
		return c ^ 7;
	}

	// color cube
	if (c < 232) {
		c -= 16;

		uint8_t r = 5 - (c / 36);
		uint8_t g = 5 - ((c / 6) % 6);
		uint8_t b = 5 - (c % 6);

		return 16 + (r * 36) + (g * 6) + b;
	}

	// grey ramp
	return 255 - (c - 232);
}

uint8_t C_ClosestRGB(int r, int g, int b) {
	// if the color is close to greyscale, limit the search to greyscale
	// colors, otherwise skip the greyscale colors.

	int greyscale = 1;
	if (abs(g - r) > 10 || abs(g - b) > 10) {
		greyscale = 0;
	}

	uint8_t best = 0;
	int     best_dist = (1 << 30);

	int c;
	for (c = 0 ; c < 256 ; c++) {
		// we compare against the original colors (without gamma)
		int r2 = palette[c][0];
		int g2 = palette[c][1];
		int b2 = palette[c][2];

		int is_grey = (r2 == g2 && g2 == b2) ? 1 : 0;
		if (is_grey != greyscale) {
			continue;
		}

		int dr = abs(r - r2);
		int dg = abs(g - g2);
		int db = abs(b - b2);

		int dist = (dr * dr) + (dg * dg) + (db * db);

		if (best_dist > dist) {
			best_dist = dist;
			best      = c;
		}
	}

	return best;
}
