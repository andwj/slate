
Slate README
============

by Andrew Apted, 2022.


About
-----

This is a terminal emulator, something akin to xterm or GNOME terminal,
or the text consoles of Linux and the BSD systems.  It supports Unicode
via the UTF-8 encoding, and uses TrueType fonts for drawing text.
The video and input is done via LibSDL (Simple Directmedia Layer).
A major goal is to be usable on Windows as well as the Linux/BSDs.


Status
------

Under development....


Documentation
-------------

TODO


Legalese
--------

Slate is under a permissive MIT license.

Slate comes with NO WARRANTY of any kind, express or implied.

See the [LICENSE.md](LICENSE.md) doc for the full terms.

