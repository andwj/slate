// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

#include "local.h"

#include "SDL_syswm.h"

#if defined(__APPLE__)
//
// MacOS
//
extern void NSBeep(void);

void A_AudibleBell(SDL_Window *window) {
	NSBeep();
}

#elif defined(_WIN32)
//
// Windows
//
void A_AudibleBell(SDL_Window *window) {
	// this parameter == a simple beep
	MessageBeep(0xFFFFFFFF);
}

#elif defined(XBELL_DYNAMIC) && defined(SDL_VIDEO_DRIVER_X11)
//
// Unix + X Windows
//

#include "dlfcn.h"

typedef int (*XBell_funcptr_t)(Display *, int);

// we use dlsym() to find the XBell function, which should exist in the
// Xlib library, which is either linked normally or dlopen'd by LibSDL.
// if we cannot find it, use a do-nothing replacement.
static XBell_funcptr_t XBell_p = NULL;

static int XBell_Dummy(Display *d, int p) {
	return 0;
}

void A_AudibleBell(SDL_Window *window) {
	if (XBell_p == NULL) {
		// using NULL here to search the whole executable
		void *handle = dlopen(NULL, RTLD_LAZY);

		if (handle != NULL) {
			XBell_p = (XBell_funcptr_t) dlsym(handle, "XBell");
			dlclose(handle);
		}

		if (XBell_p == NULL) {
			XBell_p = &XBell_Dummy;
		}
	}

	SDL_SysWMinfo wminfo;
	SDL_VERSION(&wminfo.version);

	if (! SDL_GetWindowWMInfo(window, &wminfo)) {
		return;
	}
	if (wminfo.subsystem != SDL_SYSWM_X11) {
		return;
	}

	Display *display = wminfo.info.x11.display;

	// higher values of conf.bell make the sound louder.
	// Note: XBell applies this value to a current base volume, hence
	// your milage may vary, e.g. "bell = 1" may actually be silent.
	int percent = -40;
	if (conf.bell == 2) { percent = 20; }
	if (conf.bell == 3) { percent = 80; }

	(*XBell_p)(display, percent);
}

#else
//
// a do-nothing implementation
//
void A_AudibleBell(SDL_Window *window) {
}

#endif
