// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

#include "local.h"

static void draw_point(SDL_Renderer *R, int x, int y) {
	SDL_RenderDrawPoint(R, x, y);
}

static void draw_line(SDL_Renderer *R, int x1, int y1, int x2, int y2) {
	SDL_RenderDrawLine(R, x1, y1, x2, y2);
}

static void fill_rect(SDL_Renderer *R, int x1, int y1, int x2, int y2) {
	if (x1 > x2 || y1 > y2) {
		return;
	}

	SDL_Rect rect;

	rect.x = x1;
	rect.y = y1;
	rect.w = (x2 - x1 + 1);
	rect.h = (y2 - y1 + 1);

	SDL_RenderFillRect(R, &rect);
}

//----------------------------------------------------------------------

static int quadrants[10] = {
	4, 8, 1, 13, 9,  7, 11, 2, 6, 14
};

// this table is missing four elements, the ones where the left side
// is all the same and the right side is all the same, corresponding
// to these characters: U+0020 U+258C U+2590 and U+2588.

static int sextants[60] = {
	0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A,
	0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14,

	0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F,
	0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29,

	0x2B, 0x2C, 0x2D, 0x2E, 0x2F, 0x30, 0x31, 0x32, 0x33, 0x34,
	0x35, 0x36, 0x37, 0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D, 0x3E
};

static void B_RenderQuadrant(SDL_Renderer *R, int mask, int w, int h) {
	int mx = w / 2;
	int my = h / 2;

	if (mask & 1) { fill_rect(R, 0,  0,  mx - 1, my - 1); }
	if (mask & 2) { fill_rect(R, mx, 0,   w - 1, my - 1); }
	if (mask & 4) { fill_rect(R, 0,  my, mx - 1,  h - 1); }
	if (mask & 8) { fill_rect(R, mx, my,  w - 1,  h - 1); }
}

static void B_RenderSextant(SDL_Renderer *R, int mask, int w, int h) {
	int mx = w / 2;
	int y1 = h / 3;
	int y2 = h * 2 / 3;

	if (mask & 1)  { fill_rect(R, 0,  0,  mx - 1, y1 - 1); }
	if (mask & 2)  { fill_rect(R, mx, 0,   w - 1, y1 - 1); }
	if (mask & 4)  { fill_rect(R, 0,  y1, mx - 1, y2 - 1); }
	if (mask & 8)  { fill_rect(R, mx, y1,  w - 1, y2 - 1); }
	if (mask & 16) { fill_rect(R, 0,  y2, mx - 1,  h - 1); }
	if (mask & 32) { fill_rect(R, mx, y2,  w - 1,  h - 1); }
}

static void R_RenderShaded(SDL_Renderer *R, int level, int w, int h) {
	int x, y;

	for (y = 0 ; y < h ; y++) {
		for (x = 0 ; x < w ; x++) {
			if (level == 1 && ((x | y) & 1)) continue;
			if (level == 2 && ((x ^ y) & 1)) continue;
			if (level == 3 && ((x & y) & 1)) continue;

			fill_rect(R, x, y, x, y);
		}
	}
}

//----------------------------------------------------------------------

// this table covers the range U+2500 to U+257F.
// each used entry in this table is a hex number 0xTRBL, where:
//    T = top part
//    R = right part
//    B = bottom part
//    L = left part

#define PART_EMPTY   0x0
#define PART_LIGHT   0x1
#define PART_THICK   0x2
#define PART_DOUBLE  0x3
#define PART_DASHED  0x4  // + LIGHT or THICK

static int line_drawing[128] = {
	0x0101, 0x0202, 0x1010, 0x2020, 0x0505, 0x0606, 0x5050, 0x6060,  // U+2500
	0x0505, 0x0606, 0x5050, 0x6060, 0x0110, 0x0210, 0x0120, 0x0220,
	0x0011, 0x0012, 0x0021, 0x0022, 0x1100, 0x1200, 0x2100, 0x2200,  // U+2510
	0x1001, 0x1002, 0x2001, 0x2002, 0x1110, 0x1210, 0x2110, 0x1120,

	0x2120, 0x2210, 0x1220, 0x2220, 0x1011, 0x1012, 0x2011, 0x1021,  // U+2520
	0x2021, 0x2012, 0x1022, 0x2022, 0x0111, 0x0112, 0x0211, 0x0212,
	0x0121, 0x0122, 0x0221, 0x0222, 0x1101, 0x1102, 0x1201, 0x1202,  // U+2530
	0x2101, 0x2102, 0x2201, 0x2202, 0x1111, 0x1112, 0x1211, 0x1212,

	0x2111, 0x1121, 0x2121, 0x2112, 0x2211, 0x1122, 0x1221, 0x2212,  // U+2540
	0x1222, 0x2122, 0x2221, 0x2222, 0x0505, 0x0606, 0x5050, 0x6060,
	0x0303, 0x3030, 0x0310, 0x0130, 0x0330, 0x0013, 0x0031, 0x0033,  // U+2550
	0x1300, 0x3100, 0x3300, 0x1003, 0x3001, 0x3003, 0x1310, 0x3130,

	0x3330, 0x1013, 0x3031, 0x3033, 0x0313, 0x0131, 0x0333, 0x1303,  // U+2560
	0x3101, 0x3303, 0x1313, 0x3131, 0x3333, 0x0110, 0x0011, 0x1001,
	0x1100, 0,      0,      0,      0x0001, 0x1000, 0x0100, 0x0010,  // U+2570
	0x0002, 0x2000, 0x0200, 0x0020, 0x0201, 0x1020, 0x0102, 0x2010
};

static void R_RenderBoxPart(SDL_Renderer *RE, int idx, int w, int h) {
	int pattern = line_drawing[idx];

	int T = (pattern >> (3 * 4)) & 0xF;
	int R = (pattern >> (2 * 4)) & 0xF;
	int B = (pattern >> (1 * 4)) & 0xF;
	int L = (pattern           ) & 0xF;

	// NOTE: we don't handle the THICK mode (I see little point)

	// coordinates for light segments
	int mx1 = w / 2;  int mx2 = mx1;
	int my1 = h / 2;  int my2 = my1;

	if (w > 11 && h > 15) {
		mx2 += 1; my2 += 1;
	}

	// coordinates for double segments
	int dx1 = mx1 - 1;  int dx2 = dx1;
	int ex1 = mx2 + 2;  int ex2 = ex1;

	int dy1 = my1 - 1;  int dy2 = dy1;
	int ey1 = my2 + 2;  int ey2 = ey1;

	if (w > 11 && h > 15) {
		dx1 -= 1; dy1 -= 1;
		ex1 -= 1; ey1 -= 1;
	}

	// handle dashed segments
	if ((L & PART_DASHED) != 0) {
		fill_rect(RE, 2, my1, mx1 - 2, my2);
		L = 0;
	}
	if ((R & PART_DASHED) != 0) {
		fill_rect(RE, mx2 + 2, my1, w - 3, my2);
		R = 0;
	}
	if ((T & PART_DASHED) != 0) {
		fill_rect(RE, mx1, 2, mx2, my1 - 2);
		T = 0;
	}
	if ((B & PART_DASHED) != 0) {
		fill_rect(RE, mx1, my2 + 2, mx2, h - 3);
		B = 0;
	}

	// handle double segments
	if (L == PART_DOUBLE) {
		int ax = w / 2;
		int bx = w / 2;
		if (T == PART_DOUBLE && B == PART_DOUBLE) {
			ax = dx2; bx = dx2;
		} else if (T == PART_DOUBLE) {
			ax = dx2; bx = ex2;
		} else if (B == PART_DOUBLE) {
			ax = ex2; bx = dx2;
		}
		fill_rect(RE, 0, dy1, ax, dy2);
		fill_rect(RE, 0, ey1, bx, ey2);
	}
	if (R == PART_DOUBLE) {
		int ax = w / 2;
		int bx = w / 2;
		if (T == PART_DOUBLE && B == PART_DOUBLE) {
			ax = ex1; bx = ex1;
		} else if (T == PART_DOUBLE) {
			ax = ex1; bx = dx1;
		} else if (B == PART_DOUBLE) {
			ax = dx1; bx = ex1;
		}
		fill_rect(RE, ax, dy1, w - 1, dy2);
		fill_rect(RE, bx, ey1, w - 1, ey2);
	}
	if (T == PART_DOUBLE) {
		int ay = h / 2;
		int by = h / 2;
		if (L == PART_DOUBLE && R == PART_DOUBLE) {
			ay = dy2; by = dy2;
		} else if (L == PART_DOUBLE) {
			ay = dy2; by = ey2;
		} else if (R == PART_DOUBLE) {
			ay = ey2; by = dy2;
		}
		fill_rect(RE, dx1, 0, dx2, ay);
		fill_rect(RE, ex1, 0, ex2, by);
	}
	if (B == PART_DOUBLE) {
		int ay = h / 2;
		int by = h / 2;
		if (L == PART_DOUBLE && R == PART_DOUBLE) {
			ay = ey1; by = ey1;
		} else if (L == PART_DOUBLE) {
			ay = ey1; by = dy1;
		} else if (R == PART_DOUBLE) {
			ay = dy1; by = ey1;
		}
		fill_rect(RE, dx1, ay, dx2, h - 1);
		fill_rect(RE, ex1, by, ex2, h - 1);
	}

	// handle light segments
	if (L > 0 && L != PART_DOUBLE) {
		int ax = mx2;
		if (T == PART_DOUBLE && B == PART_DOUBLE) {
			ax = dx1;
		} else if (T == PART_DOUBLE || B == PART_DOUBLE) {
			ax = ex2;
		}
		fill_rect(RE, 0, my1, ax, my2);
	}
	if (R > 0 && R != PART_DOUBLE) {
		int ax = mx1;
		if (T == PART_DOUBLE && B == PART_DOUBLE) {
			ax = ex2;
		} else if (T == PART_DOUBLE || B == PART_DOUBLE) {
			ax = dx1;
		}
		fill_rect(RE, ax, my1, w - 1, my2);
	}
	if (T > 0 && T != PART_DOUBLE) {
		int ay = my2;
		if (L == PART_DOUBLE && R == PART_DOUBLE) {
			ay = dy1;
		} else if (L == PART_DOUBLE || R == PART_DOUBLE) {
			ay = ey2;
		}
		fill_rect(RE, mx1, 0, mx2, ay);
	}
	if (B > 0 && B != PART_DOUBLE) {
		int ay = my1;
		if (L == PART_DOUBLE && R == PART_DOUBLE) {
			ay = ey2;
		} else if (L == PART_DOUBLE || R == PART_DOUBLE) {
			ay = dy1;
		}
		fill_rect(RE, mx1, ay, mx2, h - 1);
	}
}

static void R_RenderScanLine(SDL_Renderer *R, int offset, int w, int h) {
	// offset is 0..3 where 0..1 are above U+2500, and 2..3 are below it.
	// the calculations here provide consistency with U+2500 (which is
	// handled in R_RenderBoxPart above).
	if (offset >= 2) {
		offset += 1;
	}

	int my1 = (offset + 1) * h / 6;
	int my2 = my1;

	if (w > 11 && h > 15) {
		my2 += 1;
	}
	if (my2 > h - 1) {
		my2 = h - 1;
	}

	fill_rect(R, 0, my1, w - 1, my2);
}

static void R_RenderDiagonal(SDL_Renderer *R, int mask, int w, int h) {
	if (mask & 1) {
		draw_line(R, 0, 0, w - 1, h - 2);
		draw_line(R, 0, 1, w - 1, h - 1);
	}
	if (mask & 2) {
		draw_line(R, 0, h - 2, w - 1, 0);
		draw_line(R, 0, h - 1, w - 1, 1);
	}
}

//----------------------------------------------------------------------

int B_IsBlockChar(UChar ch) {
	// we handle all chars in "Box Drawing" group
	if (0x2500 <= ch && ch <= 0x257F) {
		return 1;
	}

	// we handle all chars in "Block Elements" group
	if (0x2580 <= ch && ch <= 0x259F) {
		return 1;
	}

	// we only handle SCAN LINES in the "Miscellaneous Technical" group
	if (0x23BA <= ch && ch <= 0x23BD) {
		return 1;
	}

	// we only handle SEXTANTS in the "Symbols for Legacy Computing" group
	if (0x1FB00 <= ch && ch <= 0x1FB3B) {
		return 1;
	}

	return 0;
}

void B_RenderBlockChar(SDL_Renderer *R, UChar ch, int w, int h) {
	// this assumes the background has been filled, and the drawing
	// color of the renderer is set.

	if (ch == 0x2580) {  // UPPER HALF
		fill_rect(R, 0, 0, w - 1, h / 2 - 1);
		return;
	}

	if (ch == 0x2590) {  // RIGHT HALF
		fill_rect(R, w / 2, 0, w - 1, h - 1);
		return;
	}

	if (0x2581 <= ch && ch <= 0x2588) {  // LOWER EIGHTH BLOCK
		int y = (0x2588 - ch) * h / 8;
		if (y > h - 1) y = h - 1;
		fill_rect(R, 0, y, w - 1, h - 1);
		return;
	}

	if (0x2589 <= ch && ch <= 0x258F) {  // LEFT EIGHTH BLOCK
		int x = (0x2590 - ch) * w / 8;
		if (x == 0) x = 1;
		fill_rect(R, 0, 0, x - 1, h - 1);
		return;
	}

	if (0x2591 <= ch && ch <= 0x2593) {  // SHADED BLOCK
		R_RenderShaded(R, ch - 0x2590, w, h);
		return;
	}

	if (ch == 0x2594) {  // UPPER ONE EIGHTH
		int y = h / 8;
		if (y == 0) y = 1;
		fill_rect(R, 0, 0, w - 1, y - 1);
		return;
	}

	if (ch == 0x2595) {  // RIGHT ONE EIGHTH
		int x = w * 7 / 8;
		if (x > w - 1) x = w - 1;
		fill_rect(R, x, 0, w - 1, h - 1);
		return;
	}

	if (0x2571 <= ch && ch <= 0x2573) {  // LIGHT DIAGONAL
		R_RenderDiagonal(R, ch - 0x2570, w, h);
		return;
	}

	if (0x2500 <= ch && ch <= 0x257F) {  // BOX DRAWING
		R_RenderBoxPart(R, ch - 0x2500, w, h);
		return;
	}

	if (0x23BA <= ch && ch <= 0x23BD) {  // HORIZONTAL SCAN LINE
		R_RenderScanLine(R, ch - 0x23BA, w, h);
		return;
	}

	if (0x2596 <= ch && ch <= 0x259F) {  // QUADRANTS
		B_RenderQuadrant(R, quadrants[ch - 0x2596], w, h);
		return;
	}

	if (0x1FB00 <= ch && ch <= 0x1FB3B) {  // SEXTANTS
		B_RenderSextant(R, sextants[ch - 0x1FB00], w, h);
		return;
	}
}

//----------------------------------------------------------------------

void B_RenderHorizLine(SDL_Renderer *R, int y, int w, int h) {
	if (y < h) {
		fill_rect(R, 0, y, w - 1, y);
	}
}

void B_RenderDiamond(SDL_Renderer *R, int w, int h) {
	int size = w - 2;
	if (w > h) {
		size = h - 2;
	}

	int mx = w / 2;
	int my = h / 2;
	int dy;

	for (dy = 0 ; dy <= size / 2 ; dy++) {
		int dist = size / 2 - dy;

		fill_rect(R, mx - dist, my - dy, mx + dist, my - dy);
		fill_rect(R, mx - dist, my + dy, mx + dist, my + dy);
	}
}

void B_RenderQuestionMark(SDL_Renderer *R, int w, int h) {
	int size = w - 2;
	if (w > h) {
		size = h - 2;
	}
fprintf(stderr, "SIZE %d\n", size);

	int mx = w / 2;
	int my = h / 2;

	int bx = mx;
	int by = my - size / 3;

	int ax = bx - size / 6;
	int ay = by + size / 6;

	int cx = bx + size / 6;
	int cy = ay;

	int dy = ay + size / 6;
	int ey = my + size / 8;
	int fy = my + size / 3 - 1;

	int s, t;
	int s_thick = (size >= 20) ? 2 : 1;
	int t_thick = (size >= 12) ? 2 : 1;

	for (s = 0 ; s < s_thick ; s++) {
		for (t = 0 ; t < t_thick ; t++) {
			draw_line(R, ax + s, ay + t, bx + s, by + t);
			draw_line(R, bx + s, by + t, cx + s, cy + t);
			draw_line(R, cx + s, cy + t, mx + s, dy + t);
			draw_line(R, mx + s, dy + t, mx + s, ey + t);

			draw_point(R, mx + s, fy + t);
		}
	}
}
