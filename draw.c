// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

#include "local.h"

typedef struct {
	// the size of window
	int width, height;

	SDL_Window   *window;
	SDL_Renderer *renderer;

	// whether our window has the keyboard focus
	int has_focus;

} WindowState;

static WindowState win;

//----------------------------------------------------------------------

typedef struct {
	UChar    ch;
	uint8_t  fg, bg;
	uint16_t attr;
} Cell;

typedef struct {
	Cell cells[MAX_WIDTH];
	int  dirty;  // 2 = redraw all cells
} Line;

#define MAX_CSI_PARMS   16
#define MAX_CSI_STRING  64

#define CHARSET_US   0
#define CHARSET_UK   1
#define CHARSET_GFX  2

typedef struct {
	// columns and rows
	int   w, h;

	// cursor position.
	//   x is from 0 to  w,    going across
	//   y is from 0 to (h-1), going down
	int   x, y;

	// current colors and attributes.  ch is not used
	Cell  col;

	// this is reversed indexed, 0 is the bottom-most line
	Line *lines[MAX_HEIGHT];

	// current charsets (CHARSET_XXX)
	int   g0, g1;

	// the "shift out" mode is in effect (via character U+000E)
	int   shift_out;

	// scrolling margins, normally 0 to (screen.h - 1), t < b
	int   margin_t;
	int   margin_b;

	// this is > 0 when viewing off-screen lines
	int   back_view;

	// set when something must be redrawn, 2 = redraw everything
	int   dirty;

	// usually zero, 1 = we received ESC, otherwise next char ('[' for CSI)
	int   escape;

	// numeric parameters for a CSI sequence, -1 if unset
	int   parm[MAX_CSI_PARMS];

	// this is usually zero, marks a certain special char appearing
	// in a CSI sequence, especially the '?' question mark.
	int   csi_priv;

	// current parameter being received in a CSI sequence
	char  cur_str[MAX_CSI_STRING];

	// saved cursor position, colors and attributes
	int   saved_x, saved_y;
	Cell  saved_col;
	int   saved_g0, saved_g1;
	int   saved_out;

	// flashing for the visual bell?
	int   flashing;

} ScreenState;

static ScreenState screen;


// mapping table for VT102 (etc) special character set to Unicode
// code points.  This table begins at 0x60 and ends as 0x7E, which
// includes the lowercase letters.

static const UChar special_characters[31] = {
	0x25C6, 0x2592, 0x2409, 0x240C, 0x240D, 0x240A, 0x00B0, 0x00B1,
	0x2424, 0x240B, 0x2518, 0x2510, 0x250C, 0x2514, 0x253C, 0x23BA,
	0x23BB, 0x2500, 0x23BC, 0x23BD, 0x251C, 0x2524, 0x2534, 0x252C,
	0x2502, 0x2264, 0x2265, 0x03C0, 0x2260, 0x00A3, 0x00B7
};

//----------------------------------------------------------------------

void W_ClearBackground(void) {
	SDL_SetRenderDrawColor(win.renderer, 0x00, 0x00, 0x00, 0xFF);
	SDL_RenderClear(win.renderer);
}

void W_OpenWindow(void) {
	Uint32 flags   = 0;
	Uint32 r_flags = 0;

	flags |= SDL_WINDOW_RESIZABLE;

	if (conf.fullscreen) {
		flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
		flags |= SDL_WINDOW_BORDERLESS;

		// these are probably arbitrary
		win.width  = 1920;
		win.height = 1080;
	} else {
		win.width  = conf.columns * cell_w;
		win.height = conf.rows    * cell_h;
	}

	win.window = SDL_CreateWindow(NULL,
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
		win.width, win.height, flags);

	if (win.window == NULL) {
		Die("cannot create window: %s\n", SDL_GetError());
	}

	if (conf.software > 0) {
		r_flags |= SDL_RENDERER_SOFTWARE;
	}

	win.renderer = SDL_CreateRenderer(win.window, -1, r_flags);
	if (win.renderer == NULL) {
		Die("cannot create renderer: %s\n", SDL_GetError());
	}

	SDL_GetWindowSize(win.window, &win.width, &win.height);

	SDL_SetWindowTitle(win.window, "Slate Terminal");

	// clear window to black
	W_ClearBackground();

	win.has_focus = 1;
}

void W_CloseWindow(void) {
	if (win.renderer != NULL) {
		SDL_DestroyRenderer(win.renderer);
		win.renderer = NULL;
	}
	if (win.window != NULL) {
		SDL_DestroyWindow(win.window);
		win.window = NULL;
	}
}

SDL_Texture * W_CacheCell(const Cell *c, int cursor) {
	UChar ch = c->ch;

	if (ch == 0) {
		ch = ' ';
	}
	if (c->attr & ATTR_HIDDEN) {
		ch = ' ';
	}

	if (B_IsBlockChar(ch) || ch == 0xFFFD) {
		// ok
	} else {
		// if font lacks the character, try a fallback.
		// NOTE: this is only for punctuation, especially hyphens.
		// [ we deliberately do not downgrade accented chars ]

		for (;;) {
			if (G_FontHasChar(ch)) {
				break;
			}
			UChar new_ch = T_FallbackChar(ch);
			if (new_ch != 0) {
				ch = new_ch;
				continue;
			}
			ch = 0xFFFD;
			break;
		}
	}

	// determine colors...

	uint8_t fg = c->fg;
	uint8_t bg = c->bg;

	if (c->attr & ATTR_DIM) {
		fg = C_DimColor(fg);
	}
	if (c->attr & (ATTR_REVERSE | ATTR_IS_SELECTED)) {
		fg = c->bg;
		bg = c->fg;
	}
	if (screen.flashing) {
		fg = C_OppositeColor(fg);
		bg = C_OppositeColor(bg);
	}
	if (cursor) {
		bg = conf.cursor_color;
	}

	// limit attributes to ones handled by font
	uint16_t attr = c->attr & (ATTR_BOLD | ATTR_ITALIC | ATTR_UNDERLINE | ATTR_STRIKE);

	return G_RenderCell(win.renderer, ch, fg, bg, attr);
}

void W_DrawCell(const Cell *c, int x, int y) {
	int cursor = 0;
	if (win.has_focus && screen.back_view == 0) {
		cursor = (x == screen.x && y == screen.y);
	}

	SDL_Texture *tex = W_CacheCell(c, cursor);

	SDL_Rect rect;

	rect.x = x * cell_w;
	rect.y = y * cell_h;
	rect.w = cell_w;
	rect.h = cell_h;

	SDL_RenderCopy(win.renderer, tex, NULL, &rect);
}

void W_RedrawLine(Line *L, int y) {
	int x;
	for (x = 0 ; x < screen.w ; x++) {
		Cell *c = &L->cells[x];

		if (screen.dirty >= 2 || L->dirty >= 2 || (c->attr & ATTR_IS_DIRTY)) {
			W_DrawCell(c, x, y);
		}

		c->attr &= ~ATTR_IS_DIRTY;
	}

	L->dirty = 0;
}

void W_Redraw(void) {
	if (screen.dirty == 0) {
		return;
	}

	// in software mode, our window has a persistent SDL_Surface which
	// we can rely on (I believe) to not randomly change or go away.
	// otherwise we MUST redraw the whole screen....
	if (conf.software == 0) {
		screen.dirty = 2;
	}

	if (screen.dirty >= 2) {
		W_ClearBackground();
	}

	int y;

	for (y = 0 ; y < screen.h ; y++) {
		int y2 = y + screen.back_view;

		if (screen.dirty >= 2 || screen.lines[y2]->dirty > 0) {
			W_RedrawLine(screen.lines[y2], screen.h - 1 - y);
		}
	}

	screen.dirty = 0;

	// blit to screen
	SDL_RenderPresent(win.renderer);
}

void W_Focus(int focus) {
	win.has_focus = focus;
	screen.dirty  = 2;
}

//----------------------------------------------------------------------

#define DEBUG_UNKNOWN_SEQ  0

void S_LineFeed(void);
void S_RawScrollUp(int y1, int y2);
void S_RawScrollDown(int y1, int y2);
void S_EraseInLine(int y, int x1, int x2);

void S_ResizeLine(Line *L, int new_w) {
	if (new_w < MAX_WIDTH) {
		Cell *p = &L->cells[new_w];
		size_t count = MAX_WIDTH - new_w;
		memset(p, 0, sizeof(Cell) * count);
	}
}

void S_EraseCell(Cell *c) {
	c->ch   = ' ';
	c->fg   = screen.col.fg;
	c->bg   = screen.col.bg;
	c->attr = screen.col.attr | ATTR_IS_DIRTY;
}

void S_EraseAllLines(void) {
	int x, y;
	for (y = 0 ; y < MAX_HEIGHT ; y++) {
		Line *L = screen.lines[y];

		S_ResizeLine(L, screen.w);

		for (x = 0 ; x < screen.w ; x++) {
			Cell *c = &L->cells[x];
			S_EraseCell(c);
		}
	}
	screen.dirty = 2;
}

int S_LineIsEmpty(Line *L) {
	int x;

	for (x = 0 ; x < screen.w ; x++) {
		UChar ch = L->cells[x].ch;
		if (! (ch == 0 || ch == ' ')) {
			return 0;
		}
	}
	return 1;
}

void S_PerformResize(int old_h) {
	int i;

	// if the terminal is vertically shorter, remove empty lines at the
	// bottom, otherwise just fix the cursor pos.
	if (screen.h < old_h) {
		for (i = screen.h ; i < old_h ; i++) {
			if (S_LineIsEmpty(screen.lines[0])) {
				S_RawScrollDown(0, MAX_HEIGHT-1);
			} else {
				screen.y -= 1;
			}
		}
	}

	// if the terminal is vertically taller, fill the bottom area with
	// blank lines (obtained from the very top of off-screen area).
	if (screen.h > old_h) {
		for (i = old_h ; i < screen.h ; i++) {
			S_RawScrollUp(0, MAX_HEIGHT-1);
			S_EraseInLine(screen.h - 1, 0, MAX_WIDTH-1);
		}
	}

	// clear right side of each line
	for (i = 0 ; i < MAX_HEIGHT ; i++) {
		S_ResizeLine(screen.lines[i], screen.w);
	}

	if (screen.y < 0) {
		screen.y = 0;
	}
	if (screen.y > screen.h - 1) {
		screen.h = screen.h - 1;
	}
	if (screen.x > screen.w) {
		screen.x = screen.w;
	}

	// reset scroll margins, unconditionally
	screen.margin_t = 0;
	screen.margin_b = screen.h - 1;

	screen.back_view = 0;

	// update the pseudo-terminal with the new size
	SH_SetWindowSize(screen.w, screen.h);
}

int S_ComputeSize(void) {
	int new_w = win.width  / cell_w;
	int new_h = win.height / cell_h;

	if (new_w < 1) { new_w = 1; }
	if (new_h < 1) { new_h = 1; }

	if (new_w > MAX_WIDTH)  { new_w = MAX_WIDTH;  }
	if (new_h > MAX_HEIGHT) { new_h = MAX_HEIGHT; }

	int changed = (screen.w != new_w) || (screen.h != new_h);

	screen.w = new_w;
	screen.h = new_h;

	return changed;
}

void S_WindowResize(int pixel_w, int pixel_h) {
	win.width  = pixel_w;
	win.height = pixel_h;

	int old_h = screen.h;

	if (S_ComputeSize()) {
		S_PerformResize(old_h);
	}

	screen.back_view = 0;
	screen.dirty = 2;
}

void S_ScrollBack(int delta) {
	int step  = screen.h;
	int limit = MAX_HEIGHT - screen.h - 1;

	if (step > 1) {
		step -= 1;
	}
	if (delta < 0) {
		step = -step;
	}

	screen.back_view += step;
	if (screen.back_view > limit) {
		screen.back_view = limit;
	}
	if (screen.back_view < 0) {
		screen.back_view = 0;
	}

	screen.dirty = 2;
}

void S_ResetState(void) {
	screen.col.fg   = 7;
	screen.col.bg   = 0;
	screen.col.attr = ATTR_NONE;

	screen.x  = 0;
	screen.y  = 0;
	screen.g0 = CHARSET_US;
	screen.g1 = CHARSET_US;

	// for the Linux console, G1 defaults to the graphics set
	if (conf.term[0] == 'l') {
		screen.g1 = 2;
	}

	screen.shift_out = 0;
	screen.margin_t  = 0;
	screen.margin_b  = screen.h - 1;
	screen.back_view = 0;

	screen.saved_x   = screen.x;
	screen.saved_y   = screen.y;
	screen.saved_col = screen.col;
	screen.saved_g0  = screen.g0;
	screen.saved_g1  = screen.g1;
	screen.saved_out = screen.shift_out;

	I_KeypadMode(0);
	I_CursorMode(0);
}

void S_ResetTerminal(void) {
	int old_h = screen.h;

	if (S_ComputeSize()) {
		S_PerformResize(old_h);
	}

	S_ResetState();

	// blank all lines, including scroll-back buffer
	S_EraseAllLines();

	screen.dirty = 2;
}

void S_InitTerminal(void) {
	int i;

	S_ComputeSize();

	// update the pseudo-tty with the size
	SH_SetWindowSize(screen.w, screen.h);

	// allocate the lines
	for (i = 0 ; i < MAX_HEIGHT ; i++) {
		Line *L = UT_Alloc(sizeof(Line));
		memset(L, 0, sizeof(Line));

		screen.lines[i] = L;
	}

	S_ResetState();
	S_EraseAllLines();

	screen.dirty = 2;
}

void S_EraseInLine(int y, int x1, int x2) {
	if (x1 > x2) {
		return;
	}

	Line *L = screen.lines[screen.h - 1 - y];

	int x;
	for (x = x1 ; x <= x2 ; x++) {
		Cell *c = &L->cells[x];
		S_EraseCell(c);
	}

	L->dirty     |= 1;
	screen.dirty |= 1;
}

void S_RawScrollUp(int y1, int y2) {
	// y1 and y2 are BOTTOM-UP indexes

	if (y1 >= y2) {
		return;
	}

	Line *top = screen.lines[y2];

	int y;
	for (y = y2 ; y > y1 ; y--) {
		screen.lines[y] = screen.lines[y-1];
	}

	screen.lines[y1] = top;
	screen.dirty = 2;
}

void S_RawScrollDown(int y1, int y2) {
	// y1 and y2 are BOTTOM-UP indexes

	if (y1 >= y2) {
		return;
	}

	Line *bottom = screen.lines[y1];

	int y;
	for (y = y1 ; y < y2 ; y++) {
		screen.lines[y] = screen.lines[y+1];
	}

	screen.lines[y2] = bottom;
	screen.dirty = 2;
}

void S_ScrollUp(void) {
	// if scroll margin touches top of window, then we scroll through
	// the off-screen back buffer.  otherwise it is not affected.

	int low  = screen.h - 1 - screen.margin_b;
	int high = screen.h - 1 - screen.margin_t;

	if (screen.margin_t == 0) {
		S_RawScrollUp(low, MAX_HEIGHT-1);
	} else {
		S_RawScrollUp(low, high);
	}

	S_EraseInLine(screen.margin_b, 0, screen.w - 1);
}

void S_ScrollDown(void) {
	int low  = screen.h - 1 - screen.margin_b;
	int high = screen.h - 1 - screen.margin_t;

	S_RawScrollDown(low, high);

	S_EraseInLine(screen.margin_t, 0, screen.w - 1);
}

void S_DirtyAll(void) {
	screen.dirty = 2;
}

void S_MarkDirty(int x, int y) {
	if (x >= screen.w) {
		return;
	}

	Line *L = screen.lines[screen.h - 1 - y];
	Cell *c = &L->cells[x];

	c->attr      |= ATTR_IS_DIRTY;
	L->dirty     |= 1;
	screen.dirty |= 1;
}

void S_MoveCursor(int new_x, int new_y) {
	if (new_x < 0) {
		new_x = 0;
	} else if (new_x > screen.w) {
		new_x = screen.w;
	}

	if (new_y < 0) {
		new_y = 0;
	} else if (new_y > screen.h - 1) {
		new_y = screen.h - 1;
	}

	S_MarkDirty(screen.x, screen.y);

	screen.x = new_x;
	screen.y = new_y;

	S_MarkDirty(screen.x, screen.y);
}

void S_PutChar(UChar ch) {
	if (screen.x >= screen.w) {
		S_LineFeed();
	}

	Line *L = screen.lines[screen.h - 1 - screen.y];
	Cell *c = &L->cells[screen.x];

	c->ch   = ch;
	c->bg   = screen.col.bg;
	c->fg   = screen.col.fg;
	c->attr = screen.col.attr | ATTR_IS_DIRTY;

	L->dirty     |= 1;
	screen.dirty |= 1;

	S_MoveCursor(screen.x + 1, screen.y);
}

void S_PutMark(UChar mark) {
	int x = screen.x;

	if (x == 0) {
		// no character to combine with
		return;
	}
	x -= 1;

	Line *L = screen.lines[screen.h - screen.y - 1];

	if (x > 0 && (L->cells[x-1].attr & ATTR_IS_WIDE) != 0) {
		x -= 1;
	}

	Cell *c = &L->cells[x];

	UChar new_ch = T_CombineChars(mark, c->ch);

	if (new_ch != 0) {
		c->ch   = new_ch;
		c->attr = c->attr | ATTR_IS_DIRTY;

		L->dirty     |= 1;
		screen.dirty |= 1;
	}
}

void S_LineFeed(void) {
	int new_y = screen.y + 1;

	if (new_y > screen.margin_b) {
		new_y = screen.margin_b;
		S_ScrollUp();
	}

	S_MoveCursor(0, new_y);
}

void S_VerticalTab(void) {
	int new_y = screen.y + 1;

	if (new_y > screen.margin_b) {
		new_y = screen.margin_b;
		S_ScrollUp();
	}

	S_MoveCursor(screen.x, new_y);
}

void S_CarriageReturn(void) {
	S_MoveCursor(0, screen.y);
}

static void S_VisualBell(void) {
	// higher values mean a longer delay
	int delay = 10;
	if (conf.bell == 5) { delay = 40; }
	if (conf.bell == 6) { delay = 90; }

	screen.flashing = 1;
	screen.dirty    = 2;
	W_Redraw();

	SDL_Delay(delay);

	screen.flashing = 0;
	screen.dirty    = 2;
	W_Redraw();

	SDL_Delay(delay);
}

void S_Bell(void) {
	if (1 <= conf.bell && conf.bell <= 3) {
		A_AudibleBell(win.window);
	}
	if (4 <= conf.bell && conf.bell <= 6) {
		S_VisualBell();
	}
}

void S_Backspace(void) {
	// other terminals I tested never go back to a previous line.
	if (screen.x > 0) {
		S_MoveCursor(screen.x - 1, screen.y);
	}
}

void S_Tab(void) {
	// other terminals I tested will only move to the next line when
	// cursor is past the end of current line.

	if (screen.x >= screen.w) {
		S_LineFeed();
		return;
	}

	int step  = ((screen.x & 7) ^ 7) + 1;
	int new_x = screen.x + step;

	if (new_x >= screen.w - 1) {
		new_x  = screen.w - 1;
	}

	S_MoveCursor(new_x, screen.y);
}

static void ESC_SaveState(void) {
	screen.saved_x   = screen.x;
	screen.saved_y   = screen.y;
	screen.saved_col = screen.col;
	screen.saved_g0  = screen.g0;
	screen.saved_g1  = screen.g1;
	screen.saved_out = screen.shift_out;
}

static void ESC_RestoreState(void) {
	screen.col = screen.saved_col;
	screen.g0  = screen.saved_g0;
	screen.g1  = screen.saved_g1;

	screen.shift_out = screen.saved_out;

	S_MoveCursor(screen.saved_x, screen.saved_y);
}

static void ESC_RevLineFeed(void) {
	// technically this should only scroll when cursor is at the left-most
	// column, but the Linux console is more lax, and we follow suit.

	if (screen.y > screen.margin_t) {
		S_MoveCursor(screen.x, screen.y - 1);
	} else {
		S_ScrollDown();
	}
}

static void ESC_BeginCSI(void) {
	int i;
	for (i = 0 ; i < MAX_CSI_PARMS ; i++) {
		screen.parm[i] = -1;
	}

	memset(screen.cur_str, 0, sizeof(screen.cur_str));

	screen.csi_priv = 0;
	screen.escape   = '[';
}

static void ESC_BeginOSC(void) {
	// OSC is currently ignored
}

static void CSI_DeviceStatus(void) {
	switch (screen.parm[0]) {
	case 5: // device status report
		SH_WriteStr("\033[0n");
		break;

	case 6: // cursor position report
		{
			char buffer[128];
			sprintf(buffer, "\033[%d;%dR", screen.y + 1, screen.x + 1);
			SH_WriteStr(buffer);
		}
		break;

	default:
		// ignore an unknown parameter
		break;
	}
}

static void CSI_SetAttribute(void) {
	if (screen.parm[0] == -1) {
		screen.parm[0] = 0;
	}

	int i = 0;

	while (i < MAX_CSI_PARMS) {
		int parm = screen.parm[i++];
		if (parm < 0) {
			break;
		}

		if (30 <= parm && parm <= 37) {
			screen.col.fg = parm - 30;
			continue;
		}
		if (90 <= parm && parm <= 97) {
			screen.col.fg = parm - 90 + 8;
			continue;
		}
		if (40 <= parm && parm <= 47) {
			screen.col.bg = parm - 40;
			continue;
		}
		if (100 <= parm && parm <= 107) {
			screen.col.bg = parm - 100 + 8;
			continue;
		}

		if (parm == 38 || parm == 48) {
			int color;

			switch (screen.parm[i]) {
			case 2: // a 24-bit RGB color
				if (i + 4 > MAX_CSI_PARMS) {
					return;
				}
				int r = screen.parm[i + 1];
				int g = screen.parm[i + 2];
				int b = screen.parm[i + 3];

				color = C_ClosestRGB(r, g, b);
				i += 4;
				break;

			case 5: // a 256 palette color
				if (i + 2 > MAX_CSI_PARMS) {
					return;
				}
				color = screen.parm[i + 1];
				i += 2;
				break;

			default:
				// invalid sequence, cannot recover
				return;
			}

			if (color > 255) {
				// ignore it
			} else if (parm == 38) {
				screen.col.fg = color;
			} else {
				screen.col.bg = color;
			}
			continue;
		}

		switch (parm) {
		case 0:  // reset to default
			screen.col.fg   = 7;
			screen.col.bg   = 0;
			screen.col.attr = 0;
			break;

		case 1:  // bold
			screen.col.attr |= ATTR_BOLD;
			break;

		case 2:  // dim / faint
			screen.col.attr |= ATTR_DIM;
			break;

		case 22: // disable bold and dim/faint
			screen.col.attr &= ~ATTR_BOLD;
			screen.col.attr &= ~ATTR_DIM;
			break;

		case 3:  // italics
			screen.col.attr |= ATTR_ITALIC;
			break;

		case 23: // disable italics
			screen.col.attr &= ~ATTR_ITALIC;
			break;

		case 4:  // underline
		case 21: // double underline
			screen.col.attr |= ATTR_UNDERLINE;
			break;

		case 24: // disable underline
			screen.col.attr &= ~ATTR_UNDERLINE;
			break;

		case 5:  // blink
		case 25: // disable blink
			/* ignored for the good of all humankind */
			break;

		case 7:  // inverse
			screen.col.attr |= ATTR_REVERSE;
			break;

		case 27: // disable inverse
			screen.col.attr &= ~ATTR_REVERSE;
			break;

		case 8:  // hidden
			screen.col.attr |= ATTR_HIDDEN;
			break;

		case 28: // disable hidden
			screen.col.attr &= ~ATTR_HIDDEN;
			break;

		case 9:  // strike-through
			screen.col.attr |= ATTR_STRIKE;
			break;

		case 29: // disable strike-through
			screen.col.attr &= ~ATTR_STRIKE;
			break;

		case 39: // set fg to default
			screen.col.fg = 7;
			break;

		case 49: // set bg to default
			screen.col.bg = 0;
			break;

		default:
			// ignore unknown parameter
#if DEBUG_UNKNOWN_SEQ
			fprintf(stderr, "SGR PARM %d\n", parm);
#endif
			break;
		}
	}
}

static void CSI_ScrollMargins(void) {
	int t = screen.parm[0];
	int b = screen.parm[1];

	if (t < 1) {
		t = 1;
	}
	if (b < 1 || b > screen.h) {
		b = screen.h;
	}

	// the VT220 manual says that the smallest region is two lines.
	// fbterm also moves the cursor, but xterm and st do not.

	if (t < b) {
		screen.margin_t = t - 1;
		screen.margin_b = b - 1;
	}
}

static void CSI_Move(int dx, int dy) {
	if (screen.parm[0] > 1) {
		dx = dx * screen.parm[0];
		dy = dy * screen.parm[0];
	}

	int new_x = screen.x + dx;
	int new_y = screen.y + dy;

	// on a vertical move, limit to scroll margins unless outside it.
	// this is consistent with what xterm does.

	if (dy < 0 && screen.margin_t > 0 && screen.y >= screen.margin_t) {
		if (new_y < screen.margin_t) {
			new_y = screen.margin_t;
		}
	}
	if (dy > 0 && screen.margin_b < (screen.h - 1) && screen.y <= screen.margin_b) {
		if (new_y > screen.margin_b) {
			new_y = screen.margin_b;
		}
	}

	S_MoveCursor(new_x, new_y);
}

static void CSI_MoveCol(void) {
	int new_x = screen.parm[0] - 1;
	S_MoveCursor(new_x, screen.y);
}

static void CSI_MoveRow(void) {
	int new_y = screen.parm[1] - 1;
	S_MoveCursor(screen.x, new_y);
}

static void CSI_MoveRowCol(void) {
	int new_y = screen.parm[0] - 1;
	int new_x = screen.parm[1] - 1;
	S_MoveCursor(new_x, new_y);
}

static void CSI_Erase(void) {
	int y = screen.y;

	if (screen.parm[0] == 3) {
		S_EraseAllLines();
	}

	if (screen.parm[0] >= 2) {
		for (y = 0 ; y < screen.h ; y++) {
			S_EraseInLine(y, 0, screen.w - 1);
		}
		return;
	}

	if (screen.parm[0] == 1) {
		// erase from start of screen to cursor
		S_EraseInLine(y, 0, screen.x);

		for (y-- ; y >= 0 ; y--) {
			S_EraseInLine(y, 0, screen.w - 1);
		}
	} else {
		// erase from cursor to end of screen
		S_EraseInLine(y, screen.x, screen.w - 1);

		for (y++ ; y < screen.h ; y++) {
			S_EraseInLine(y, 0, screen.w - 1);
		}
	}
}

static void CSI_EraseLine(void) {
	int x1 = 0;
	int x2 = screen.w - 1;

	if (screen.parm[0] <= 0) {
		x1 = screen.x;
	} else if (screen.parm[0] == 1) {
		x2 = screen.x;
	}

	S_EraseInLine(screen.y, x1, x2);
}

static void CSI_EraseChar(void) {
	int count = screen.parm[0];
	if (count < 1) {
		count = 1;
	}

	int x1 = screen.x;
	int x2 = x1 + count - 1;

	if (x2 > screen.w - 1) {
		x2 = screen.w - 1;
	}

	S_EraseInLine(screen.y, x1, x2);
}

static void CSI_InsertLine(void) {
	// the current line (and those below it) are pushed down.

	int count = screen.parm[0];
	if (count < 1) {
		count = 1;
	}

	int y;
	int ytop = screen.h - 1 - screen.y;

	// check if we actually need to move lines down...
	if (ytop < count) {
		for (y = 0 ; y <= ytop ; y++) {
			S_EraseInLine(screen.h - 1 - y, 0, screen.w - 1);
		}
		S_MoveCursor(0, screen.y);
		return;
	}

	// move lines down...
	int n;
	for (n = 0 ; n < count ; n++) {
		S_RawScrollDown(0, ytop);
	}

	for (y = 0 ; y < count ; y++) {
		S_EraseInLine(screen.y + y, 0, screen.w - 1);
	}
	for (y = 0 ; y <= ytop ; y++) {
		screen.lines[y]->dirty = 2;
	}

	S_MoveCursor(0, screen.y);

	screen.dirty |= 1;
}

static void CSI_DeleteLine(void) {
	int count = screen.parm[0];
	if (count < 1) {
		count = 1;
	}

	int y;
	int ytop = screen.h - 1 - screen.y;

	// check if we actually need to move lines up...
	if (ytop < count) {
		for (y = 0 ; y <= ytop ; y++) {
			S_EraseInLine(screen.h - 1 - y, 0, screen.w - 1);
		}
		S_MoveCursor(0, screen.y);
		return;
	}

	// move lines up...
	int n;
	for (n = 0 ; n < count ; n++) {
		S_RawScrollUp(0, ytop);
	}

	for (y = 0 ; y < count ; y++) {
		S_EraseInLine(screen.h - 1 - y, 0, screen.w - 1);
	}
	for (y = 0 ; y <= ytop ; y++) {
		screen.lines[y]->dirty = 2;
	}

	S_MoveCursor(0, screen.y);

	screen.dirty |= 1;
}

static void CSI_InsertChar(void) {
	// the character at the cursor (etc) are pushed to the right.

	int count = screen.parm[0];
	if (count < 1) {
		count = 1;
	}

	int ytop = screen.h - 1 - screen.y;
	Line *L  = screen.lines[ytop];

	int x;
	for (x = screen.w - 1 ; x - count >= screen.x ; x--) {
		L->cells[x] = L->cells[x - count];
		L->cells[x].attr |= ATTR_IS_DIRTY;
	}
	S_EraseInLine(screen.y, screen.x, screen.x + count - 1);

	L->dirty     |= 1;
	screen.dirty |= 1;
}

static void CSI_DeleteChar(void) {
	int count = screen.parm[0];
	if (count < 1) {
		count = 1;
	}

	int ytop = screen.h - 1 - screen.y;
	Line *L  = screen.lines[ytop];

	// shift cells to the left
	int x;
	for (x = screen.x ; x + count < screen.w ; x++) {
		L->cells[x] = L->cells[x + count];
		L->cells[x].attr |= ATTR_IS_DIRTY;
	}
	S_EraseInLine(screen.y, x, screen.w - 1);

	L->dirty     |= 1;
	screen.dirty |= 1;
}

static void S_ControlChar(UChar ch) {
	switch (ch) {
	case '\000': /* NUL */ break;
	case '\007': /* BEL */ S_Bell();             break;
	case '\010': /* BS  */ S_Backspace();        break;
	case '\011': /* TAB */ S_Tab();              break;
	case '\012': /* LF  */ S_LineFeed();         break;
	case '\013': /* VT  */ S_VerticalTab();      break;
	case '\014': /* FF  */ S_VerticalTab();      break;
	case '\015': /* CR  */ S_CarriageReturn();   break;
	case '\016': /* SO  */ screen.shift_out = 1; break;
	case '\017': /* SI  */ screen.shift_out = 0; break;
	case '\033': /* ESC */ screen.escape = 1;    break;

	case 0x0084: /* IND */ S_VerticalTab();      break;
	case 0x0085: /* NEL */ S_LineFeed();         break;
	case 0x008D: /* RI  */ ESC_RevLineFeed();    break;
	case 0x009B: /* CSI */ ESC_BeginCSI();       break;
	case 0x009D: /* OSC */ ESC_BeginOSC();       break;

	default:
		// ignore anything we don't know
#if DEBUG_UNKNOWN_SEQ
		fprintf(stderr, "CONTROL CHAR: 0x%02x\n", ch);
#endif
		break;
	}
}

static void S_ProcessEscapeChar(UChar ch) {
	screen.escape = 0;

	switch (ch) {
	case 'c': /* RIS */     S_ResetTerminal();  return;
	case '[': /* CSI */     ESC_BeginCSI();     return;
	case ']': /* OSC */     ESC_BeginOSC();     return;
	case '=': /* DECKPAM */ I_KeypadMode(1);    return;
	case '>': /* DECKPNM */ I_KeypadMode(0);    return;
	case '7': /* DECSC */   ESC_SaveState();    return;
	case '8': /* DECRC */   ESC_RestoreState(); return;
	case 'Z': /* DECID */   I_TerminalID();     return;
	default: break;
	}

	// character set selection (DECSCS)
	switch (ch) {
	case '(': case ')': case '+': case '-':
	case '.': case '/':
		screen.escape = (int)ch;
		return;

	default: break;
	}

	if (0x40 <= ch && ch <= 0x5F) {
		S_ControlChar(ch + 0x40);
		return;
	}

	// we don't handle anything else

#if DEBUG_UNKNOWN_SEQ
	fprintf(stderr, "ESC U+00%02x", ch);
#endif
}

static void S_ParameterChar(UChar ch) {
	if ('0' <= ch && ch <= '9') {
		size_t p = strlen(screen.cur_str);

		if (p + 1 < MAX_CSI_STRING) {
			screen.cur_str[p]   = (char)ch;
			screen.cur_str[p+1] = 0;
		}
		return;
	}

	if (ch == ';' || ch == ':') {
		size_t p = 0;
		while (screen.parm[p] >= 0 && p < MAX_CSI_PARMS) {
			p++;
		}
		if (p < MAX_CSI_PARMS) {
			int value = 0;
			if (screen.cur_str[0] != 0) {
				value = atoi(screen.cur_str);
			}
			screen.parm[p] = value;
		}
		// begin a new parameter
		screen.cur_str[0] = 0;
		return;
	}

	if (ch == '<' || ch == '=' || ch == '>' || ch == '?') {
		screen.csi_priv = ch;
	}
}

static void S_DumpCSISequence(UChar ch) {
#if DEBUG_UNKNOWN_SEQ
	fprintf(stderr, "CSI ");
	{
		if (screen.csi_priv != 0) {
			fprintf(stderr, "%c ", screen.csi_priv);
		}

		int k;
		for (k = 0 ; k < MAX_CSI_PARMS ; k++) {
			if (screen.parm[k] < 0) break;
			fprintf(stderr, "%d ", screen.parm[k]);
		}
	}
	fprintf(stderr, "%c\n", (int)ch);
#endif
}

static void S_ProcessCSIChar(UChar ch) {
	// simply ignore any "intermediate" bytes
	if (ch <= 0x2F) {
		if (screen.csi_priv < 0x30) {
			screen.csi_priv = ch;
		}
		return;
	}

	// process parameter bytes
	if (ch <= 0x3F) {
		S_ParameterChar(ch);
		return;
	}

	// check the final byte to see what command to invoke
	screen.escape = 0;

	// finish any pending parameter
	if (screen.cur_str[0] != 0) {
		S_ParameterChar(';');
	}

	// handle some private sequences
	if (screen.csi_priv != 0) {
		// TODO 'h' and 'l'

		S_DumpCSISequence(ch);
		return;
	}

	switch (ch) {
	case 'A': /* CUU */   CSI_Move(0, -1);     break;
	case 'B': /* CUD */   CSI_Move(0, +1);     break;
	case 'C': /* CUF */   CSI_Move(+1, 0);     break;
	case 'D': /* CUB */   CSI_Move(-1, 0);     break;
	case 'E': /* CNL */   CSI_Move(-999, +1);  break;
	case 'F': /* CPL */   CSI_Move(-999, -1);  break;
	case 'G': /* CHA */   CSI_MoveCol();       break;
	case 'H': /* CUP */   CSI_MoveRowCol();    break;

	case '@': /* ICH */   CSI_InsertChar();    break;
	case 'J': /* ED */    CSI_Erase();         break;
	case 'K': /* EL */    CSI_EraseLine();     break;
	case 'L': /* IL */    CSI_InsertLine();    break;
	case 'M': /* DL */    CSI_DeleteLine();    break;
	case 'P': /* DCH */   CSI_DeleteChar();    break;
	case 'X': /* ECH */   CSI_EraseChar();     break;

	case 'a': /* HPR */     CSI_Move(+1, 0);     break;
	case 'c': /* DA */      I_TerminalID();      break;
	case 'd': /* VPA */     CSI_MoveRow();       break;
	case 'e': /* VPR */     CSI_Move(0, +1);     break;
	case 'f': /* HVP */     CSI_MoveRowCol();    break;
	case 'm': /* SGR */     CSI_SetAttribute();  break;
	case 'n': /* DSR */     CSI_DeviceStatus();  break;
	case 'r': /* DECSTBM */ CSI_ScrollMargins(); break;
	case 's': /* SCOSC */   ESC_SaveState();     break;
	case 'u': /* SCORC */   ESC_RestoreState();  break;
	case '`': /* HPA */     CSI_MoveCol();       break;

	default:
		// ignore an unknown CSI sequence
		S_DumpCSISequence(ch);
	}
}

static void S_ProcessSCSChar(UChar ch) {
	int charset = 0;

	// the VT220 supports many more charsets, for French, German, Swedish,
	// Italian, etc....  but I don't think they are worth emulating.

	switch (ch) {
	case 'A': charset = CHARSET_UK;  break;
	case 'B': charset = CHARSET_US;  break;
	case '0': charset = CHARSET_GFX; break;
	default: break;
	}

	switch (screen.escape) {
	case '(': screen.g0 = charset; break;
	case ')': screen.g1 = charset; break;
	default: break;
	}

	screen.escape = 0;
}

void S_ProcessChar(UChar ch) {
	if (screen.escape == 1) {
		S_ProcessEscapeChar(ch);
		return;
	}

	if (screen.escape == '[') {
		if (0x20 <= ch && ch <= 0x7E) {
			S_ProcessCSIChar(ch);
			return;
		}

		// character was illegal for CSI, it breaks the sequence
		screen.escape = 0;
	}

	if (screen.escape != 0) {
		S_ProcessSCSChar(ch);
		return;
	}

	if (ch < 32 || (0x80 <= ch && ch <= 0x9F)) {
		S_ControlChar(ch);
		return;
	}

	// if viewing off-screen lines, jump to bottom
	if (screen.back_view > 0) {
		screen.back_view = 0;
		screen.dirty = 2;
	}

	// convert all the unicode space chars into ' '
	if (T_IsSpace(ch)) {
		S_PutChar(' ');
		return;
	}

	// handle combining characters (usually diacritic marks)
	if (T_IsMark(ch)) {
		S_PutMark(ch);
		return;
	}

	// a zero-width character can be ignored
	switch (ch) {
	case 0x200B: case 0x200C:
	case 0x200D: case 0xFEFF:
		return;

	default: break;
	}

	// if otherwise undisplayable, produce the <?> symbol
	if (! T_IsPrint(ch)) {
		S_PutChar(0xFFFD);
		return;
	}

	// handle the current charset
	int charset = screen.shift_out ? screen.g1 : screen.g0;

	if (charset == CHARSET_UK) {
		if (ch == '$') {
			ch = 0xA3;  // pound sign
		}
	}
	if (charset == CHARSET_GFX) {
		if (0x60 <= ch && ch <= 0x7E) {
			ch = special_characters[ch - 0x60];
		}
	}

	S_PutChar(ch);
}

//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
