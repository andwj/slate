// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

#include "local.h"

typedef struct {
	SDL_Keycode   code;
	Uint32        flags;
	const char  * str;
} KeyString;

// flag bits:
#define KS_SHIFT     (1 << 0)   // require SHIFT modifier
#define KS_ALT       (1 << 1)   // require ALT   modifier
#define KS_CTRL      (1 << 2)   // require CTRL  modifier
#define KS_META      (1 << 4)   // require META  modifier

#define KS_ANY_MOD   (1 << 8)   // allow any modifier key

#define KS_APPL_KP   (1 << 10)  // application mode of keypad
#define KS_APPL_CUR  (1 << 11)  // application mode of cursor keys
#define KS_NO_LOCK   (1 << 12)  // require NUM_LOCK be off


// the keyboard strings support a few '%' escapes:
//   %% : is needed to get a percent sign
//   %m : if modifiers are pressed, it inserts a semicolon and then a number:
//        1 + (shift ? 1 : 0) + (alt ? 2 : 0) + (ctrl ? 4 : 0) + (meta ? 8 : 0)
//   %M : same as %m but inserts a '1' before the semicolon.


static KeyString vt102_table[] = {
	// the VT102 has a DELETE key which produces the '\177' char.
	// it has no INSERT key or anything like PGUP and PGDN.

	{ SDLK_DELETE,  0,  "\177" },

	// Note that VT102 don't have HOME and END keys.
	// the following are what xterm generates.

	{ SDLK_HOME,   KS_APPL_CUR,  "\033OH"  },
	{ SDLK_END,    KS_APPL_CUR,  "\033OF"  },

	{ SDLK_HOME,   0,  "\033[H"  },
	{ SDLK_END,    0,  "\033[F"  },

	// these are the PF1 .. PF4 keys at top of the keypad
	{ SDLK_F1,     0,  "\033OP" },
	{ SDLK_F2,     0,  "\033OQ" },
	{ SDLK_F3,     0,  "\033OR" },
	{ SDLK_F4,     0,  "\033OS" },

	// no real key, these map to the `4 5 6 ,` row on the keypad.
	{ SDLK_F5,     0,  "\033Ot" },
	{ SDLK_F6,     0,  "\033Ou" },
	{ SDLK_F7,     0,  "\033Ov" },
	{ SDLK_F8,     0,  "\033Ol" },

	// no real key, these map to the `7 8 9 -` row on the keypad.
	{ SDLK_F9,     0,  "\033Ow" },
	{ SDLK_F10,    0,  "\033Ox" },
	{ SDLK_F11,    0,  "\033Oy" },
	{ SDLK_F12,    0,  "\033Om" },

	{ 0, 0, NULL }
};

static KeyString vt220_table[] = {
	// the VT220 has a DELETE key which produces the '\177' char, and a
	// REMOVE key (in the editing group) which produces a CSI sequence.
	// we cannot emulate both, so we use the CSI sequence.

	{ SDLK_FIND,      KS_ANY_MOD,  "\033[1%m~" },
	{ SDLK_INSERT,    KS_ANY_MOD,  "\033[2%m~" },
	{ SDLK_DELETE,    KS_ANY_MOD,  "\033[3%m~" },
	{ SDLK_SELECT,    KS_ANY_MOD,  "\033[4%m~" },
	{ SDLK_PAGEUP,    KS_ANY_MOD,  "\033[5%m~" },
	{ SDLK_PAGEDOWN,  KS_ANY_MOD,  "\033[6%m~" },

	// the VT220 doesn't have HOME and END keys.
	// however, we map the HOME key to FIND, and END key to SELECT
	// (those keys appear in the key group above the cursor keys).
	// the Linux console uses these sequences too.

	{ SDLK_HOME,  KS_ANY_MOD,  "\033[1%m~" },
	{ SDLK_END,   KS_ANY_MOD,  "\033[4%m~" },

	// Note that a real VT220 does not have F1..F5 per se, instead it has
	// five keys which do not send codes (like HOLD, PRINT, and BREAK).

	// with no modifier, these four are same as VT102.
	{ SDLK_F1,   0,  "\033OP" },
	{ SDLK_F2,   0,  "\033OQ" },
	{ SDLK_F3,   0,  "\033OR" },
	{ SDLK_F4,   0,  "\033OS" },

	// with a modifier, these four replace SS3 of the above with CSI
	// (probably an xterm innovation).
	{ SDLK_F1,   KS_ANY_MOD,  "\033[1%mP" },
	{ SDLK_F2,   KS_ANY_MOD,  "\033[1%mQ" },
	{ SDLK_F3,   KS_ANY_MOD,  "\033[1%mR" },
	{ SDLK_F4,   KS_ANY_MOD,  "\033[1%mS" },

	{ SDLK_F5,   KS_ANY_MOD,  "\033[15%m~" },
	{ SDLK_F6,   KS_ANY_MOD,  "\033[17%m~" },
	{ SDLK_F7,   KS_ANY_MOD,  "\033[18%m~" },
	{ SDLK_F8,   KS_ANY_MOD,  "\033[19%m~" },
	{ SDLK_F9,   KS_ANY_MOD,  "\033[20%m~" },
	{ SDLK_F10,  KS_ANY_MOD,  "\033[21%m~" },
	{ SDLK_F11,  KS_ANY_MOD,  "\033[23%m~" },
	{ SDLK_F12,  KS_ANY_MOD,  "\033[24%m~" },

	{ SDLK_F13,  KS_ANY_MOD,  "\033[25%m~" },
	{ SDLK_F14,  KS_ANY_MOD,  "\033[26%m~" },
	{ SDLK_F15,  KS_ANY_MOD,  "\033[28%m~" },
	{ SDLK_F16,  KS_ANY_MOD,  "\033[29%m~" },
	{ SDLK_F17,  KS_ANY_MOD,  "\033[31%m~" },
	{ SDLK_F18,  KS_ANY_MOD,  "\033[32%m~" },
	{ SDLK_F19,  KS_ANY_MOD,  "\033[33%m~" },
	{ SDLK_F20,  KS_ANY_MOD,  "\033[34%m~" },

	// the VT220 has a HELP key in top row (where F15 should be).
	// it also has a DO key where F16 should be.
	{ SDLK_HELP,    KS_ANY_MOD,  "\033[28%m~" },
	{ SDLK_EXECUTE, KS_ANY_MOD,  "\033[29%m~" },

	{ 0, 0, NULL }
};

static KeyString linux_table[] = {
	// the Linux console generates '\177' for backspace.
	// xterm does too (for what it's worth...)

	{ SDLK_BACKSPACE, KS_ANY_MOD, "\177" },

	{ SDLK_F1,   0,  "\033[[A" },
	{ SDLK_F2,   0,  "\033[[B" },
	{ SDLK_F3,   0,  "\033[[C" },
	{ SDLK_F4,   0,  "\033[[D" },
	{ SDLK_F5,   0,  "\033[[E" },

	// F6..F12 are same as VT220 (via another table)

	// with shift key, these are F13..F20 of the VT220.
	{ SDLK_F1,   KS_SHIFT,  "\033[25~" },
	{ SDLK_F2,   KS_SHIFT,  "\033[26~" },
	{ SDLK_F3,   KS_SHIFT,  "\033[28~" },
	{ SDLK_F4,   KS_SHIFT,  "\033[29~" },
	{ SDLK_F5,   KS_SHIFT,  "\033[31~" },
	{ SDLK_F6,   KS_SHIFT,  "\033[32~" },
	{ SDLK_F7,   KS_SHIFT,  "\033[33~" },
	{ SDLK_F8,   KS_SHIFT,  "\033[34~" },

	// these produce nothing on the Linux console.
	{ SDLK_F9,   KS_SHIFT,  "" },
	{ SDLK_F10,  KS_SHIFT,  "" },
	{ SDLK_F11,  KS_SHIFT,  "" },
	{ SDLK_F12,  KS_SHIFT,  "" },

	// these codes are produced when NUMLOCK is off, and are just
	// mappings to what the cursor and editing keys generate.
	// xterm does different things for KP7, KP1, and punctuation.

	{ SDLK_KP_0,        KS_NO_LOCK | KS_ANY_MOD,  "\033[2%m~" },  // INS
	{ SDLK_KP_PERIOD,   KS_NO_LOCK | KS_ANY_MOD,  "\033[3%m~" },  // DEL
	{ SDLK_KP_7,        KS_NO_LOCK | KS_ANY_MOD,  "\033[1%m~" },  // HOME
	{ SDLK_KP_1,        KS_NO_LOCK | KS_ANY_MOD,  "\033[4%m~" },  // END
	{ SDLK_KP_9,        KS_NO_LOCK | KS_ANY_MOD,  "\033[5%m~" },  // PGUP
	{ SDLK_KP_3,        KS_NO_LOCK | KS_ANY_MOD,  "\033[6%m~" },  // PGDN

	{ SDLK_KP_2,        KS_NO_LOCK | KS_ANY_MOD,  "\033[%MA"  },  // UP
	{ SDLK_KP_8,        KS_NO_LOCK | KS_ANY_MOD,  "\033[%MB"  },  // DOWN
	{ SDLK_KP_6,        KS_NO_LOCK | KS_ANY_MOD,  "\033[%MC"  },  // RIGHT
	{ SDLK_KP_4,        KS_NO_LOCK | KS_ANY_MOD,  "\033[%MD"  },  // LEFT
	{ SDLK_KP_5,        KS_NO_LOCK | KS_ANY_MOD,  "\033[%ME"  },  // BEGIN

	{ 0, 0, NULL }
};

static KeyString appl_keypad_table[] = {
	// the VT102 and VT220 lack `+`, `*` or `/` on the keypad.

	{ SDLK_KP_0,        KS_APPL_KP,  "\033Op" },
	{ SDLK_KP_1,        KS_APPL_KP,  "\033Oq" },
	{ SDLK_KP_2,        KS_APPL_KP,  "\033Or" },
	{ SDLK_KP_3,        KS_APPL_KP,  "\033Os" },
	{ SDLK_KP_4,        KS_APPL_KP,  "\033Ot" },
	{ SDLK_KP_5,        KS_APPL_KP,  "\033Ou" },
	{ SDLK_KP_6,        KS_APPL_KP,  "\033Ov" },
	{ SDLK_KP_7,        KS_APPL_KP,  "\033Ow" },
	{ SDLK_KP_8,        KS_APPL_KP,  "\033Ox" },
	{ SDLK_KP_9,        KS_APPL_KP,  "\033Oy" },

	{ SDLK_KP_COMMA,    KS_APPL_KP,  "\033Ol" },
	{ SDLK_KP_PERIOD,   KS_APPL_KP,  "\033On" },
	{ SDLK_KP_MINUS,    KS_APPL_KP,  "\033Om" },
	{ SDLK_KP_ENTER,    KS_APPL_KP,  "\033OM" },

	{ 0, 0, NULL }
};

static KeyString common_table[] = {
	// the VT102 and VT220 produce '\010' (CTRL-H) for backspace.

	{ SDLK_BACKSPACE,    KS_ANY_MOD,  "\b" },
	{ SDLK_RETURN,       KS_ANY_MOD,  "\n" },
	{ SDLK_TAB,          0,           "\t" },
	{ SDLK_ESCAPE,       0,           "\033" },

	{ SDLK_UP,     KS_APPL_CUR,  "\033OA" },
	{ SDLK_DOWN,   KS_APPL_CUR,  "\033OB" },
	{ SDLK_RIGHT,  KS_APPL_CUR,  "\033OC" },
	{ SDLK_LEFT,   KS_APPL_CUR,  "\033OD" },

	{ SDLK_UP,     KS_ANY_MOD,  "\033[%MA" },
	{ SDLK_DOWN,   KS_ANY_MOD,  "\033[%MB" },
	{ SDLK_RIGHT,  KS_ANY_MOD,  "\033[%MC" },
	{ SDLK_LEFT,   KS_ANY_MOD,  "\033[%MD" },

	// need KS_ANY_MOD here to allow plain SHIFT to generate them
	{ SDLK_KP_0,         KS_ANY_MOD,  "0"  },
	{ SDLK_KP_1,         KS_ANY_MOD,  "1"  },
	{ SDLK_KP_2,         KS_ANY_MOD,  "2"  },
	{ SDLK_KP_3,         KS_ANY_MOD,  "3"  },
	{ SDLK_KP_4,         KS_ANY_MOD,  "4"  },
	{ SDLK_KP_5,         KS_ANY_MOD,  "5"  },
	{ SDLK_KP_6,         KS_ANY_MOD,  "6"  },
	{ SDLK_KP_7,         KS_ANY_MOD,  "7"  },
	{ SDLK_KP_8,         KS_ANY_MOD,  "8"  },
	{ SDLK_KP_9,         KS_ANY_MOD,  "9"  },

	{ SDLK_KP_PERIOD,    KS_ANY_MOD,  "."  },
	{ SDLK_KP_MINUS,     KS_ANY_MOD,  "-"  },
	{ SDLK_KP_PLUS,      KS_ANY_MOD,  "+"  },
	{ SDLK_KP_DIVIDE,    KS_ANY_MOD,  "/"  },
	{ SDLK_KP_MULTIPLY,  KS_ANY_MOD,  "*"  },
	{ SDLK_KP_COMMA,     KS_ANY_MOD,  ","  },
	{ SDLK_KP_EQUALS,    KS_ANY_MOD,  "="  },

	{ SDLK_KP_SPACE,     KS_ANY_MOD,  " "  },
	{ SDLK_KP_BACKSPACE, KS_ANY_MOD,  "\b" },
	{ SDLK_KP_TAB,       KS_ANY_MOD,  "\t" },
	{ SDLK_KP_ENTER,     KS_ANY_MOD,  "\n" },

	{ 0, 0, NULL }
};

//----------------------------------------------------------------------

#define MAX_KEY_STRINGS  512
#define MAX_KEY_TABLES   64

typedef struct {
	const char * id;  // identity string
	KeyString    *tables[MAX_KEY_TABLES];
} KeyboardInfo;

static KeyboardInfo vt102_terminal = {
	// id
	"\033[?6c",

	// tables
	{
		appl_keypad_table,
		vt102_table,
		common_table,
		NULL
	}
};

static KeyboardInfo vt220_terminal = {
	// id
	"\033[?62;1;2;6;c",

	// tables
	{
		appl_keypad_table,
		vt220_table,
		common_table,
		NULL
	}
};

static KeyboardInfo linux_terminal = {
	// id (same as the VT102)
	"\033[?6c",

	// tables
	{
		appl_keypad_table,
		linux_table,
		vt220_table,
		common_table,
		NULL
	}
};

typedef struct {
	// current terminal info
	KeyboardInfo * info;

	// 0 = numeric mode, 1 = application mode
	int   keypad_mode;

	// 0 = normal, 1 = application mode
	int   cursor_mode;

	// 0 = off, 1 = the NUMLOCK key is pressed (and no SHIFT)
	int   num_lock;

	// when   1, we are waiting for two keys to compose.
	// when > 1, we have the first key and are waiting for second.
	UChar  compose;

} KeyboardState;

static KeyboardState kbd;

//----------------------------------------------------------------------

void I_InitKeyboard(void) {
	// if we recognize the terminal name, we are good.
	// if we don't know it, then fall back to a default.

	// terminal names can have suffixes after a `-` dash, remove it
	char name[64];
	size_t i;

	for (i = 0 ; ; i++) {
		char ch = conf.term[i];
		if (ch == 0 || ch == '-') {
			name[i] = 0;
			break;
		}
		name[i] = tolower(ch);
	}

	if (strcmp(name, "linux") == 0) {
		kbd.info = &linux_terminal;

	} else if (strcmp(name, "ansi") == 0) {
		kbd.info = &vt102_terminal;

	} else if (strcmp(name, "vt52") == 0) {
		// VT52 is not supported

	} else if (name[0] == 'v' && name[1] == 't') {
		if (conf.term[2] == '1') {
			kbd.info = &vt102_terminal;
		} else if (conf.term[2] >= '2') {
			kbd.info = &vt220_terminal;
		}
	}

	if (kbd.info == NULL) {
		// give a warning...
		fprintf(stderr, "slate: unknown terminal name '%s', assuming a VT102\n", name);
		kbd.info = &vt102_terminal;
	}
}

void I_TerminalID(void) {
	SH_WriteStr(kbd.info->id);
}

void I_KeypadMode(int appl_mode) {
	kbd.keypad_mode = appl_mode;
}

void I_CursorMode(int appl_mode) {
	kbd.cursor_mode = appl_mode;
}

static UChar ShiftedLetter(UChar ch) {
	// TODO support all unicode letters

	if ('a' <= ch && ch <= 'z') {
		return ch - 32;
	}

	return ch;
}

static UChar ShiftedSymbol(UChar ch) {
	// UK keyboard?
	if (conf.keyboard == 1) {
		switch (ch) {
		case '2':  return '"';
		case '3':  return 0xA3;  // pound sign
		case '\'': return '@';
		case '#':  return '~';
		case '`':  return 0xAC;  // negation symbol
		default: break;
		}
	}

	switch (ch) {
	case '1':  return '!';
	case '2':  return '@';
	case '3':  return '#';
	case '4':  return '$';
	case '5':  return '%';
	case '6':  return '^';
	case '7':  return '&';
	case '8':  return '*';
	case '9':  return '(';
	case '0':  return ')';

	case '`':  return '~';
	case '-':  return '_';
	case '=':  return '+';
	case '\\': return '|';
	case '[':  return '{';
	case ']':  return '}';
	case ';':  return ':';
	case '\'': return '"';
	case ',':  return '<';
	case '.':  return '>';
	case '/':  return '?';
	case '@':  return '\'';

	default: return ch;
	}
}

static void I_WriteKeyString(const char *s, int mods) {
	char buffer[256];
	char *b = buffer;

	while (*s != 0) {
		char ch = *s++;

		if (ch != '%') {
			*b++ = ch;
			continue;
		}

		ch = *s++;

		switch (ch) {
		case 'm':
		case 'M':
			if (mods > 0) {
				if (ch == 'M') *b++ = '1';
				*b++ = ';';
				snprintf(b, 64, "%d", 1 + mods);
				b = b + strlen(b);
			}
			break;

		default:
			*b++ = ch;
		}
	}

	*b = 0;

	SH_WriteStr(buffer);
}

static int I_SpecialKeyInTable(KeyString *tab, SDL_Keycode code, Uint16 flags) {
	for (;;) {
		KeyString * key = tab; tab++;
		if (key->code == 0) {
			break;
		}

		if (key->code != code) {
			continue;
		}

		// check that modifiers match
		Uint32 mods = key->flags;

		if ((mods & KS_ANY_MOD) != 0) {
			// ok
		} else if ((mods & 0x0F) != (flags & 0x0F)) {
			continue;
		}

		if ((mods & KS_APPL_KP) != 0) {
			if (kbd.keypad_mode == 0) {
				continue;
			}
		}
		if ((mods & KS_APPL_CUR) != 0) {
			if (kbd.cursor_mode == 0) {
				continue;
			}
		}
		if ((mods & KS_NO_LOCK) != 0) {
			if (kbd.num_lock) {
				continue;
			}
		}

		// found it
		I_WriteKeyString(key->str, (int)(flags & 0x0F));
		return 1;
	}

	return 0;
}

static void I_SpecialKey(SDL_Keycode code, Uint16 flags) {
	int i = 0;

	while (kbd.info->tables[i] != NULL) {
		if (I_SpecialKeyInTable(kbd.info->tables[i], code, flags)) {
			return;
		}
		i++;
	}
}

static void I_ControlKey(UChar ch) {
	if ('a' <= ch && ch <= 'z') {
		SH_Write(ch & 31);
		return;
	}

	switch (ch) {
	case '@':  case '2': SH_Write(0);  break;
	case '\'': case '`': SH_Write(0);  break;
	case '[':            SH_Write(27); break;
	case '\\':           SH_Write(28); break;
	case ']':            SH_Write(29); break;
	case '^':  case '6': SH_Write(30); break;
	case '/':  case '?': SH_Write(31); break;

	default: break;
	}
}

static void I_GraphicSymbol(UChar ch) {
	// these will act as a dead key (next key finishes it).
	// first five are consistent with the Microsoft "US International"
	// keyboard layout, the rest are custom additions.

	switch (ch) {
	case '\'': kbd.compose = U_ACUTE;      return;
	case '`':  kbd.compose = U_GRAVE;      return;
	case '"':  kbd.compose = U_UMLAUT;     return;
	case '^':  kbd.compose = U_CIRCUMFLEX; return;
	case '~':  kbd.compose = U_TILDE;      return;

	case 'V':
	case 'v':  kbd.compose = U_HACEK;      return;
	case '_':  kbd.compose = U_MACRON;     return;
	case '.':  kbd.compose = U_OVERDOT;    return;
	case '@':  kbd.compose = U_RING;       return;
	case '(':  kbd.compose = U_BREVE;      return;

	default: break;
	}

	// these keys are currently free, but may be useful as dead keys for
	// some things we don't support yet (cedilla, ogonek, double acute,
	// belowdot, stroke/bar).

	switch (ch) {
	case '*':  //
	case '%':  // these are currently free
	case '&':  //
	case '>':  //
		SH_Write(ch);
		return;

	default: break;
	}

	// the following table follows the Microsoft "US International"
	// characters available via the AltGr key (as per Wikipedia).

	switch (ch) {
	case '1': SH_Write(0x00A1); /* inverted !     */ return;
	case '2': SH_Write(0x00B2); /* superscript 2  */ return;
	case '3': SH_Write(0x00B3); /* superscript 3  */ return;
	case '4': SH_Write(0x00A4); /* currency sign  */ return;
	case '5': SH_Write(0x20AC); /* euro sign      */ return;
	case '6': SH_Write(0x00BC); /* 1/4 fraction   */ return;
	case '7': SH_Write(0x00BD); /* 1/2 fraction   */ return;
	case '8': SH_Write(0x00BE); /* 3/4 fraction   */ return;
	case '9': SH_Write(0x2018); /* open quote     */ return;
	case '0': SH_Write(0x2019); /* close quote    */ return;

	case '!': SH_Write(0x00B9); /* superscript 1  */ return;
	case '$': SH_Write(0x00A3); /* pound sign     */ return;
	case '/': SH_Write(0x00BF); /* inverted ?     */ return;
	case '-': SH_Write(0x00A5); /* yen sign       */ return;
	case '=': SH_Write(0x00D7); /* multiply sign  */ return;
	case '+': SH_Write(0x00F7); /* division sign  */ return;
	case ';': SH_Write(0x00B6); /* pilcrow        */ return;
	case ':': SH_Write(0x00B0); /* degrees sign   */ return;
	case '|': SH_Write(0x00A6); /* broken bar     */ return;
	case '[': SH_Write(0x00AB); /* << quote       */ return;
	case ']': SH_Write(0x00BB); /* >> qupte       */ return;
	case '\\':SH_Write(0x00AC); /* negation       */ return;

	case 'a': SH_Write(0x00E1); /* a acute */ return;
	case 'A': SH_Write(0x00C1);               return;
	case 'e': SH_Write(0x00E9); /* e acute */ return;
	case 'E': SH_Write(0x00C9);               return;
	case 'i': SH_Write(0x00ED); /* i acute */ return;
	case 'I': SH_Write(0x00CD);               return;
	case 'o': SH_Write(0x00F3); /* o acute */ return;
	case 'O': SH_Write(0x00D3);               return;
	case 'u': SH_Write(0x00FA); /* u acute */ return;
	case 'U': SH_Write(0x00DA);               return;

	case ',': SH_Write(0x00E7); /* c cedilla   */ return;
	case '<': SH_Write(0x00C7);                   return;
	case 'd': SH_Write(0x00F0); /* eth         */ return;
	case 'D': SH_Write(0x00D0);                   return;
	case 'l': SH_Write(0x00F8); /* o stroke    */ return;
	case 'L': SH_Write(0x00D8);                   return;
	case 'n': SH_Write(0x00F1); /* n tilde     */ return;
	case 'N': SH_Write(0x00D1);                   return;
	case 'p': SH_Write(0x00F6); /* o umlaut    */ return;
	case 'P': SH_Write(0x00D6);                   return;
	case 'q': SH_Write(0x00E4); /* a umlaut    */ return;
	case 'Q': SH_Write(0x00C4);                   return;
	case 't': SH_Write(0x00FE); /* thorn       */ return;
	case 'T': SH_Write(0x00DE);                   return;
	case 'w': SH_Write(0x00E5); /* a ring      */ return;
	case 'W': SH_Write(0x00C5);                   return;
	case 'y': SH_Write(0x00FC); /* u umlaut    */ return;
	case 'Y': SH_Write(0x00DC);                   return;
	case 'z': SH_Write(0x00E6); /* ae ligature */ return;
	case 'Z': SH_Write(0x00C6);                   return;

	case 'c': SH_Write(0x00A9); /* (C) copyright  */ return;
	case 'C': SH_Write(0x00A2); /* cent sign      */ return;
	case 'm': SH_Write(0x00B5); /* micro sign     */ return;
	case 'r': SH_Write(0x00AE); /* (R) registered */ return;
	case 's': SH_Write(0x00DF); /* sharp s        */ return;
	case 'S': SH_Write(0x00A7); /* section sign   */ return;

	default: break;
	}

	// custom additions to the above...

	switch (ch) {
	case ' ': SH_Write(0x00A0); /* non-break space */ return;
	case '#': SH_Write(0x00B1); /* +/- sign        */ return;
	case '{': SH_Write(0x201C); /* open  dbl quote */ return;
	case '}': SH_Write(0x201D); /* close dbl quote */ return;
	case ')': SH_Write(0x2126); /* ohm sign        */ return;
	case '?': SH_Write(0x03BB); /* greek lambda    */ return;
	case 'R': SH_Write(0x2122); /* trade mark      */ return;

	case 'f': SH_Write(0x00E8); /* e grave     */  return;
	case 'F': SH_Write(0x00C8);                    return;
	case 'g': SH_Write(0x014B); /* eng         */  return;
	case 'G': SH_Write(0x014A);                    return;
	case 'h': SH_Write(0x0127); /* h stroke    */  return;
	case 'H': SH_Write(0x0126);                    return;
	case 'j': SH_Write(0x0133); /* ij ligature */  return;
	case 'J': SH_Write(0x0132);                    return;
	case 'k': SH_Write(0x0153); /* oe ligature */  return;
	case 'K': SH_Write(0x0152);                    return;
	case 'b': SH_Write(0x0142); /* l stroke     */ return;
	case 'B': SH_Write(0x0141);                    return;

	case 'x': // these are currently free
	case 'X': //

	default: break;
	}

	// no mapping, hence no change
	SH_Write(ch);
}

static void I_AltMetaKey(UChar ch, int conf_mode) {
	switch (conf_mode) {
	case 0:
		// ignore the modifier, send plain key
		SH_Write(ch);
		break;

	case 1:
		// prefix the key with ESC
		SH_Write(27);
		SH_Write(ch);
		break;

	case 2:
		I_GraphicSymbol(ch);
		break;

	default:
		// do nothing at all, eat the key
		break;
	}
}

static void I_ComposeKey(UChar c2) {
	UChar c1 = kbd.compose;
	kbd.compose = 0;

	UChar out = T_CombineChars(c1, c2);
	if (out != 0) {
		SH_Write(out);
		return;
	}

	// if first is a composing char, send it after second char
	if (T_IsMark(c1)) {
		SH_Write(c2);
		SH_Write(c1);
		return;
	}

	// unknown compose, send original keys
	SH_Write(c1);
	SH_Write(c2);
}

static void I_HandleKey(const SDL_Event *ev) {
	SDL_Keycode sym = ev->key.keysym.sym;
	Uint16      mod = ev->key.keysym.mod;

//DEBUG
//	fprintf(stderr, "GOT KEY: sym=U+%04x mode=%04x\n", sym, mod);

	// ignore events from modifier keys, and unknown keys.
	// [ otherwise it can mess up dead keys and compose ]
	switch (sym) {
	case SDLK_LCTRL:  case SDLK_RCTRL:
	case SDLK_LSHIFT: case SDLK_RSHIFT:
	case SDLK_LALT:   case SDLK_RALT:
	case SDLK_LGUI:   case SDLK_RGUI:
	case SDLK_MODE:   case SDLK_CAPSLOCK:
	case SDLK_UNKNOWN:
		return;
	}

	int is_shift  = (mod & KMOD_SHIFT) ? 1 : 0;
	int is_ctrl   = (mod & KMOD_CTRL)  ? 1 : 0;
	int is_alt    = (mod & KMOD_ALT)   ? 1 : 0;
	int is_meta   = (mod & KMOD_GUI)   ? 1 : 0;

	int caps_lock = (mod & KMOD_CAPS)  ? 1 : 0;
	int num_lock  = (mod & KMOD_NUM)   ? 1 : 0;

	kbd.num_lock = (num_lock != is_shift);

	// ALT + F4 quits unconditionally
	if (is_alt && sym == SDLK_F4) {
		fprintf(stderr, "Quit via ALT-F4 keypress.\n");
		want_quit = 1;
		return;
	}

	// use scrollback?
	if (is_shift && ! (is_alt || is_meta)) {
		if (sym == SDLK_PAGEUP) {
			S_ScrollBack(+1);
			return;
		}
		if (sym == SDLK_PAGEDOWN) {
			S_ScrollBack(-1);
			return;
		}
	}

	int special_key = (sym < ' ' || sym >= SDLK_SCANCODE_MASK);
	if (sym == SDLK_DELETE) {
		special_key = 1;
	}

	// cancel compose on a special key (and eat that key)
	if (kbd.compose > 0 && (special_key || is_ctrl || is_alt || is_meta)) {
		kbd.compose = 0;
		return;
	}

	if (special_key) {
		// override for BACKSPACE key
		if (conf.backspace > 0 && sym == SDLK_BACKSPACE) {
			SH_Write(conf.backspace);
			return;
		}

		// override for DELETE key
		if (conf.delete > 0 && sym == SDLK_DELETE) {
			SH_Write(conf.delete);
			return;
		}

		Uint32 flags = 0;

		if (is_shift) flags |= KS_SHIFT;
		if (is_alt)   flags |= KS_ALT;
		if (is_ctrl)  flags |= KS_CTRL;
		if (is_meta)  flags |= KS_META;

		I_SpecialKey(sym, flags);
		return;
	}

	/* handle normal (Unicode) chars */

	if (is_ctrl) {
		I_ControlKey(sym);
		return;
	}

	// shift + letters
	if (is_shift != caps_lock) {
		sym = ShiftedLetter(sym);
	}

	// shift + digits and symbols
	if (is_shift) {
		sym = ShiftedSymbol(sym);
	}

	if (is_alt) {
		int altgr_mode = conf.altgr;
		if (altgr_mode == 0) {
			altgr_mode = conf.alt;
		}

		if ((mod & KMOD_LALT) != 0) {
			I_AltMetaKey(sym, conf.alt);
		} else if ((mod & KMOD_RALT) != 0) {
			I_AltMetaKey(sym, altgr_mode);
		}
		return;

	} else if (is_meta) {
		I_AltMetaKey(sym, conf.meta);
		return;
	}

	if (kbd.compose == 1) {
		// await the second key...
		kbd.compose = sym;
		return;
	}
	if (kbd.compose > 1) {
		I_ComposeKey(sym);
		return;
	}

	SH_Write(sym);
}

static void I_Quit(void) {
	if (! want_quit) {
		fprintf(stderr, "Quit via window system.\n");
		want_quit |= 1;
	}
}

int I_PollInput(void) {
	int seen = 0;

	SDL_Event event;

	while (SDL_PollEvent(&event)) {
		seen = 1;

		switch (event.type) {
		case SDL_KEYDOWN:
			I_HandleKey(&event);
			break;

		case SDL_QUIT:
			I_Quit();
			break;

		case SDL_WINDOWEVENT:
			switch (event.window.event) {
			case SDL_WINDOWEVENT_CLOSE:
				I_Quit();
				break;

			case SDL_WINDOWEVENT_SHOWN:
			case SDL_WINDOWEVENT_EXPOSED:
				S_DirtyAll();
				break;

			case SDL_WINDOWEVENT_SIZE_CHANGED:
				S_WindowResize(event.window.data1, event.window.data2);
				break;

			case SDL_WINDOWEVENT_FOCUS_GAINED:
				W_Focus(1);
				break;

			case SDL_WINDOWEVENT_FOCUS_LOST:
				W_Focus(0);
				break;

			default: break;
			}
			break;

		default:
			// ignore everything else
			break;
		}
	}

	return seen;
}

void I_WaitAnyKey(void) {
	while (1) {
		SDL_Event event;

		if (SDL_WaitEvent(&event) < 0) {
			break;
		}
		if (event.type == SDL_KEYDOWN || event.type == SDL_QUIT) {
			break;
		}
	}
}

//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
