// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

// this needed for setenv() and unsetenv()
#define _POSIX_C_SOURCE  200112L

#include "local.h"

#ifndef __WIN32__

// UNIX crud
#include <unistd.h>
#include <signal.h>
#include <pty.h>
#include <termios.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/wait.h>

// arguments to the shell program, first entry is program itself
static char ** shell_args;

static pid_t shell_pid;
static int   shell_fd;

void SH_SpecifyCommand(int argc, char *argv[]) {
	// make a copy of the arguments, add a trailing NULL
	shell_args = (char **) UT_Alloc((argc + 1) * sizeof(char *));

	int i;
	for (i = 0 ; i < argc ; i++) {
		shell_args[i] = UT_Strdup(argv[i]);
	}
	shell_args[argc] = NULL;
}

static void SH_ExecChild(int child_fd) {
	// create a new session
	setsid();

	// set controlling tty for the session
	ioctl(child_fd, TIOCSCTTY, NULL);

	// set standard file handles to the PTY slave
	dup2(child_fd, 0);  // STDIN
	dup2(child_fd, 1);  // STDOUT
	dup2(child_fd, 2);  // STDERR

	close(child_fd);

	// init some important environment vars
	unsetenv("COLUMNS");
	unsetenv("LINES");
	unsetenv("TERMCAP");

	setenv("TERM", conf.term, 1);

	if (conf.shell != NULL) {
		setenv("SHELL", conf.shell, 1);
	} else {
		// leave existing value as-is
	}

	// reset various signals to their default behavior.
	// this matches what xterm and st does.  it is really only for
	// ignored signals, since execve will reset any caught signals.
	signal(SIGCHLD, SIG_DFL);
	signal(SIGHUP,  SIG_DFL);
	signal(SIGINT,  SIG_DFL);
	signal(SIGQUIT, SIG_DFL);
	signal(SIGTERM, SIG_DFL);
	signal(SIGALRM, SIG_DFL);

	// determine command if not explicitly given
	if (shell_args != NULL) {
		execvp(shell_args[0], shell_args);
	} else {
		static char *args[4];

		args[0] = conf.shell;
		args[1] = NULL;

		if (args[0] == NULL) {
			args[0] = getenv("SHELL");
		}
		if (args[0] == NULL) {
			args[0] = (char *)"/bin/sh";
		}

		execvp(args[0], args);
	}

	// only make it here when execvp fails
	Die("execute the shell failed\n");
}

void SH_ExecShell(void) {
	int parent_fd, child_fd;

	if (openpty(&parent_fd, &child_fd, NULL, NULL, NULL) < 0) {
		Die("openpty failed\n");
	}

	pid_t pid = fork();
	switch (pid) {
	case -1:
		Die("fork failed\n");
		break;

	case 0: // the child
		close(parent_fd);
		SH_ExecChild(child_fd);
		break;

	default: // the parent
		close(child_fd);
		break;
	}

	shell_pid = pid;
	shell_fd  = parent_fd;

	fcntl(shell_fd, F_SETFL, O_NONBLOCK);
}

int SH_HasTerminated(void) {
	int status;

	if (waitpid(shell_pid, &status, WNOHANG) == shell_pid) {
		fprintf(stderr, "Shell has terminated, status=%d\n", status);
		return 1;
	}

	return 0;
}

void SH_SetWindowSize(int w, int h) {
	struct winsize size;

	size.ws_col    = w;
	size.ws_row    = h;
	size.ws_xpixel = 0;
	size.ws_ypixel = 0;

	ioctl(shell_fd, TIOCSWINSZ, &size);
}

void SH_SendBreak(void) {
	tcsendbreak(shell_fd, 0);
}

//----------------------------------------------------------------------

#define READ_SIZE  4096

// when read_len is > 0 on entry to SH_Read(), it means that the buffer a
// small amount of "unfinished" data (e.g. a non-terminated CSI sequence).
static char    read_buffer[READ_SIZE];
static ssize_t read_len;

static ssize_t SH_FinishedSize(void) {
	ssize_t p = 0;
	ssize_t q;

	while (p < read_len) {
		// two byte UTF-8 code?
		if ((read_buffer[p] & 0xE0) == 0xC0) {
			if (p + 1 >= read_len) {
				return p;
			}
			p += 2;
			continue;
		}

		// three byte UTF-8 code?
		if ((read_buffer[p] & 0xF0) == 0xE0) {
			if (p + 2 >= read_len) {
				return p;
			}
			p += 3;
			continue;
		}

		// four byte UTF-8 code?
		if ((read_buffer[p] & 0xF8) == 0xF0) {
			if (p + 3 >= read_len) {
				return p;
			}
			p += 4;
			continue;
		}

		// escape sequence?
		if (read_buffer[p] == '\033') {
			q = p + 1;
			if (q >= read_len) {
				return p;
			}
			// TODO other stuff? (OSC..ST, SOS..ST etc)

			// CSI or similar?
			if (read_buffer[q] == '[' ||
				read_buffer[q] == 'N' ||
				read_buffer[q] == 'O') {

				q++;
				for (;;) {
					if (q >= read_len) {
						return p;
					}
					if (read_buffer[q] >= 0x40) {
						q++;
						break;
					}
					// don't allow the CSI string to be too long
					if (q - p >= 40) {
						break;
					}
					q++;
				}
			}

			p = q;
			continue;
		}

		// a normal ASCII character
		p++;
	}

	return read_len;
}

static void SH_Decode(ssize_t usable) {
	ssize_t p = 0;

	while (p < usable) {
		UChar ch = read_buffer[p];
		p++;

		// two byte UTF-8 code?
		if ((ch & 0xE0) == 0xC0) {
			ch  = (ch & 0x1F) << 6;
			ch |= (read_buffer[p] & 0x3F);

			S_ProcessChar(ch);
			p += 1;
			continue;
		}

		// three byte UTF-8 code?
		if ((ch & 0xF0) == 0xE0) {
			ch  = (ch & 0x1F) << 12;
			ch |= (read_buffer[p]   & 0x3F) << 6;
			ch |= (read_buffer[p+1] & 0x3F);

			S_ProcessChar(ch);
			p += 2;
			continue;
		}

		// four byte UTF-8 code?
		if ((ch & 0xF8) == 0xF0) {
			ch  = (ch & 0x0F) << 18;
			ch |= (read_buffer[p]   & 0x3F) << 12;
			ch |= (read_buffer[p+1] & 0x3F) << 6;
			ch |= (read_buffer[p+2] & 0x3F);

			S_ProcessChar(ch);
			p += 3;
			continue;
		}

		// a normal ASCII character
		S_ProcessChar(ch);
	}
}

int SH_Read(void) {
	// the previous call may have had an unfinished sequence, this will be
	// indicated by having read_len > 0.  we need to append to that text.

	ssize_t got = read(shell_fd, &read_buffer[read_len], READ_SIZE-2-read_len);
	if (got < 0) {
		if (errno == EAGAIN) { return 0; }

		// one way to get here is when shell has exited and we didn't
		// catch it yet in SH_HasTerminated().

/* DEBUG */
fprintf(stderr, "read errno = %d\n", errno);
fprintf(stderr, "ReadShell FAIL\n");

		return 1;
	}

	if (got == 0) {
		return 0;
	}

	read_len = read_len + got;
	read_buffer[read_len] = 0;

//DEBUG
//	printf("ReadShell: %s\n", read_buffer);

	ssize_t usable = SH_FinishedSize();

	if (usable > 0) {
		SH_Decode(usable);

		if (usable < read_len) {
			// shift the unfinished portion to beginning of buffer
			ssize_t left = read_len - usable;
			memmove(&read_buffer[0], &read_buffer[usable], left);
			read_len = left;

		} else {
			read_len = 0;
		}
	}

	return 1;
}

//----------------------------------------------------------------------

static ssize_t SH_Encode(char *buf, UChar ch) {
	// we don't care about UTF-16 surrogates here

	if (ch > 0x10FFFF) {
		return -1;
	}

	if (ch > 0xFFFF) {
		// four bytes are required
		*buf++ = ((ch >> 18) & 0x07) | 0xF0;
		*buf++ = ((ch >> 12) & 0x3F) | 0x80;
		*buf++ = ((ch >>  6) & 0x3F) | 0x80;
		*buf   = ((ch      ) & 0x3F) | 0x80;

		return 4;
	}

	if (ch > 0x07FF) {
		// three bytes are required
		*buf++ = ((ch >> 12) & 0x0F) | 0xE0;
		*buf++ = ((ch >>  6) & 0x3F) | 0x80;
		*buf   = ((ch      ) & 0x3F) | 0x80;

		return 3;
	}

	if (ch > 0x007F) {
		// two bytes are required
		*buf++ = ((ch >>  6) & 0x1F) | 0xC0;
		*buf   = ((ch      ) & 0x3F) | 0x80;

		return 2;
	}

	// just a single byte
	*buf = ch;
	return 1;
}

void SH_WriteStr(const char *s) {
	ssize_t res = write(shell_fd, s, strlen(s));
	if (res < 0) {
fprintf(stderr, "write errno = %d\n", errno);
		fprintf(stderr, "WriteShell FAIL\n");
	}
}

void SH_Write(UChar ch) {
	char buffer[64];

	// convert char to UTF-8
	ssize_t count = SH_Encode(buffer, ch);
	if (count < 0) {
		// invalid character
		return;
	}

	ssize_t res = write(shell_fd, buffer, count);
	if (res < 0) {
fprintf(stderr, "write errno = %d\n", errno);
		fprintf(stderr, "WriteShell FAIL\n");
	}
}

#endif

//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
