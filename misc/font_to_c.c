//
// convert a binary font file to C code.
//
// this code is licensed as CC0 (public domain).
//
// usage: font_to_c FILE.TTF > FILE.c
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define MAX_ABBREV  64

FILE *fp = NULL;
char *filename = NULL;
char  abbrev_name[MAX_ABBREV];
int   length  = 0;
int   curline = 0;

void abbreviate_filename() {
	// skip backwards to find a path separator
	char * p = filename + strlen(filename) - 1;

	while (p > filename) {
		if (p[-1] == '\\' || p[-1] == '/') {
			break;
		}
		p--;
	}

	int count = 0;

	while (*p != 0 && *p != '.' && count < (MAX_ABBREV - 2)) {
		int ch = (unsigned char) *p++;

		if (isalnum(ch)) {
			abbrev_name[count++] = ch;
		} else {
			abbrev_name[count++] = '_';
		}
	}

	abbrev_name[count] = 0;
}

void write_header(void) {
	printf("/* generated by font_to_c (in misc directory) */\n");
	printf("#ifdef EMBEDDED_FONTS\n");
	printf("#include \"local.h\"\n");
	printf("static const unsigned char filedata_%s[] = {\n", abbrev_name);
}

void write_a_byte(int b) {
	if (curline == 0) {
		printf("\t");
	}
	printf("0x%02x,", b);

	curline += 1;
	if (curline >= 16) {
		printf("\n");
		curline = 0;
	}
}

void write_data(void) {
	for (;;) {
		int ch = getc(fp);
		if (ch == EOF) {
			break;
		}
		write_a_byte(ch);
		length += 1;
	}

	if (curline > 0) {
		printf("\n");
	}
}

void write_footer(void) {
	printf("};\n");
	printf("\n");
	printf("const EmbeddedFont font_%s = {\n", abbrev_name);
	printf("\t%d, filedata_%s\n", length, abbrev_name);
	printf("};\n");
	printf("#endif /* EMBEDDED_FONTS */\n");
}

int main(int argc, char *argv[]) {
	if (argc < 2) {
		fprintf(stderr, "usage: font_to_c FILE.TTF > FILE.c\n");
		fflush(stdout);
		return 1;
	}

	filename = argv[1];

	fp = fopen(filename, "rb");
	if (fp == NULL) {
		fprintf(stderr, "cannot open file: %s\n", filename);
		fflush(stdout);
		return 1;
	}

	abbreviate_filename();

	write_header();
	write_data();
	write_footer();

	fclose(fp);
	fflush(stdout);
	return 0;
}
