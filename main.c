// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

#include "local.h"

#define VERSION_STR  "0.3.1415"

int want_quit;

Config conf;

static void InitConfig(void) {
	memset(&conf, 0, sizeof(conf));

#ifdef EMBEDDED_FONTS
	conf.font = (char *)"mono";
#else
	conf.font = (char *)"fonts/Go-Mono.ttf";
#endif

	conf.size     = 20;
	conf.columns  = 80;
	conf.rows     = 25;

	conf.alt      = 1;   // ESC prefix
	conf.meta     = 2;   // graphic symbols

	conf.gamma    = 100;
	conf.delay    = 20;
	conf.bell     = 1;
	conf.hinting  = 2;   // normal

	conf.width_frac  = 65;
	conf.width_add   = 1;
	conf.height_frac = 120;
	conf.height_add  = 1;

	conf.cursor_color = 160;

	conf.max_cache = 16;  // MB
}

static void FinalizeConfig(void) {
	if (conf.term == NULL) {
#ifdef _WIN32
		conf.term = (char *)"vt102";
#else
		conf.term = (char *)"linux";
#endif
	}
}

static void ParseGeometry(char * geom) {
	// fullscreen mode?
	if (geom[0] == 'f' || geom[0] == 'F') {
		conf.fullscreen = 1;
		return;
	}

	conf.fullscreen = 0;

	// it should be of the form: "80x25"
	if (sscanf(geom, "%dx%d", &conf.columns, &conf.rows) != 2 ||
		conf.columns < MIN_WIDTH  || conf.columns > MAX_WIDTH ||
		conf.rows    < MIN_HEIGHT || conf.rows    > MAX_HEIGHT) {

		Die("invalid geometry: '%s'\n", geom);
	}
}

static void ShowHelp(void) {
	printf("Usage: slate [OPTIONS] [[-e] command args...]\n");
	printf("\n");

	printf("Available options:\n");
	printf("   -f  --font      <file>    font file to load\n");
	printf("   -s  --size      <num>     character size of font\n");
	printf("   -g  --geometry  <WxH>     columns and rows, or \"fullscreen\"\n");
	printf("   -T  --title     <string>  title for the terminal window\n");
	printf("       --shell     <name>    default shell to run,  set SHELL var\n");
	printf("       --term      <name>    desired terminal type, set TERM  var\n");
	printf("       --CONFVAR   <value>   set other configuration settings\n");
	printf("\n");
	printf("   -h  --help                show this help text\n");
	printf("       --version             show the version\n");

	fflush(stdout);
	exit(0);
}

static void ShowVersion(void) {
	printf("Slate " VERSION_STR "\n");
	fflush(stdout);
	exit(0);
}

static int ParseConfigEntry(char *name, char *value);

static void ParseArguments(int argc, char *argv[]) {
	// skip program itself
	argv++; argc--;

	while (argc > 0) {
		char *arg = *argv;

		// handle end of arguments

		if (arg[0] != '-') {
			SH_SpecifyCommand(argc, argv);
			return;
		}

		argv++; argc--;

		if (strcmp(arg, "--") == 0) {
			if (argc > 0) {
				SH_SpecifyCommand(argc, argv);
			}
			return;
		}

		if (UT_MatchArg(arg, 'e', "execute")) {
			if (argc == 0) {
				Die("missing command after %s\n", arg);
			}
			SH_SpecifyCommand(argc, argv);
			return;
		}

		// handle arguments which do not have any parameter

		if (UT_MatchArg(arg, 'h', "help")) {
			ShowHelp();
			return;
		}
		if (UT_MatchArg(arg, 0, "version")) {
			ShowVersion();
			return;
		}

		// handle arguments which need a parameter

		char * parm = NULL;
		if (argc > 0) {
			if ((*argv)[0] != '-') {
				parm = *argv++; argc--;
			}
		}

		if (UT_MatchArg(arg, 'f', "font")) {
			if (parm == NULL || strlen(parm) == 0) {
				Die("missing value for %s option\n", arg);
			}
			conf.font = UT_Strdup(parm);
			continue;
		}
		if (UT_MatchArg(arg, 's', "size")) {
			if (parm == NULL || strlen(parm) == 0) {
				Die("missing value for %s option\n", arg);
			}
			conf.size = atoi(parm);
			continue;
		}
		if (UT_MatchArg(arg, 'g', "geometry")) {
			if (parm == NULL || strlen(parm) == 0) {
				Die("missing value for %s option\n", arg);
			}
			ParseGeometry(parm);
			continue;
		}
		if (UT_MatchArg(arg, 'T', "title")) {
			// we allow an empty string for the title
			if (parm == NULL) {
				Die("missing value for %s option\n", arg);
			}
			conf.title = UT_Strdup(parm);
			continue;
		}
		if (UT_MatchArg(arg, 0, "shell")) {
			if (parm == NULL || strlen(parm) == 0) {
				Die("missing value for %s option\n", arg);
			}
			conf.shell = UT_Strdup(parm);
			continue;
		}
		if (UT_MatchArg(arg, 0, "term")) {
			if (parm == NULL || strlen(parm) == 0) {
				Die("missing value for %s option\n", arg);
			}
			conf.term = UT_Strdup(parm);
			continue;
		}

		// anything else should be a config setting
		if (strlen(arg) <= 2) {
			Die("unknown option: %s\n", arg);
		}
		if (parm == NULL) {
			Die("missing value after %s\n", arg);
		}

		arg++;
		if (*arg == '-') {
			arg++;
		}

		if (! ParseConfigEntry(arg, parm)) {
			Die("unknown config variable '%s'\n", arg);
		}
	}
}

//----------------------------------------------------------------------

#define MAX_CONFIG_LINE  4096

static char * ConfigFileName(void) {
	// support override via the environment
	char * filename = getenv("SLATE_CONFIG");

	if (filename != NULL) {
		// disable the config file if empty
		if (strlen(filename) == 0) {
			return NULL;
		}
		return filename;
	}

	char * dir = getenv("HOME");

	if (dir == NULL) {
		dir = getenv("USERPROFILE");
	}
	if (dir == NULL) {
		return NULL;
	}

#ifndef _WIN32
	dir = UT_JoinPath(dir, ".config");
	UT_MakeDir(dir);
#endif

	dir = UT_JoinPath(dir, "slate");
	UT_MakeDir(dir);

	return UT_JoinPath(dir, "slate.cfg");
}

static int ParseConfigEntry(char *name, char *value) {
	if (UT_Compare(name, "size") == 0) {
		conf.size = atoi(value);
		return 1;
	}
	if (UT_Compare(name, "geometry") == 0) {
		ParseGeometry(value);
		return 1;
	}
	if (UT_Compare(name, "backspace") == 0) {
		conf.backspace = atoi(value);
		return 1;
	}
	if (UT_Compare(name, "delete") == 0) {
		conf.delete = atoi(value);
		return 1;
	}
	if (UT_Compare(name, "alt") == 0) {
		conf.alt = atoi(value);
		return 1;
	}
	if (UT_Compare(name, "altgr") == 0) {
		conf.altgr = atoi(value);
		return 1;
	}
	if (UT_Compare(name, "meta") == 0) {
		conf.meta = atoi(value);
		return 1;
	}
	if (UT_Compare(name, "keyboard") == 0) {
		conf.keyboard = atoi(value);
		return 1;
	}
	if (UT_Compare(name, "gamma") == 0) {
		conf.gamma = atoi(value);
		return 1;
	}
	if (UT_Compare(name, "delay") == 0) {
		conf.delay = atoi(value);
		return 1;
	}
	if (UT_Compare(name, "bell") == 0) {
		conf.bell = atoi(value);
		return 1;
	}
	if (UT_Compare(name, "software") == 0) {
		conf.software = atoi(value);
		return 1;
	}
	if (UT_Compare(name, "hinting") == 0) {
		conf.hinting = atoi(value);
		return 1;
	}
	if (UT_Compare(name, "width_frac") == 0) {
		conf.width_frac = atoi(value);
		return 1;
	}
	if (UT_Compare(name, "width_add") == 0) {
		conf.width_add = atoi(value);
		return 1;
	}
	if (UT_Compare(name, "height_frac") == 0) {
		conf.height_frac = atoi(value);
		return 1;
	}
	if (UT_Compare(name, "height_add") == 0) {
		conf.height_add = atoi(value);
		return 1;
	}
	if (UT_Compare(name, "cursor_color") == 0) {
		conf.cursor_color = atoi(value);
		return 1;
	}
	if (UT_Compare(name, "max_cache") == 0) {
		conf.max_cache = atoi(value);
		return 1;
	}

	// the remaining options want an empty string to become NULL
	if (strlen(value) == 0) {
		value = NULL;
	} else {
		value = UT_Strdup(value);
	}

	if (UT_Compare(name, "font") == 0) {
		conf.font = value;
		return 1;
	}
	if (UT_Compare(name, "title") == 0) {
		conf.title = value;
		return 1;
	}
	if (UT_Compare(name, "shell") == 0) {
		conf.shell = value;
		return 1;
	}
	if (UT_Compare(name, "term") == 0) {
		conf.term = value;
		return 1;
	}

	return 0;  // unknown entry
}

static int ParseConfigLine(FILE *fp) {
	static char line[MAX_CONFIG_LINE];

	if (! fgets(line, MAX_CONFIG_LINE, fp)) {
		// probably EOF, possibly a read error
		return 0;
	}

	char *p = line;

	// skip a Unicode BOM (byte-order mark)
	if (UT_HasBOM(p)) {
		p += 3;
	}

	// skip whitespace
	while (*p != 0 && isspace(*p)) {
		p++;
	}

	// a blank line or comment?
	if (*p == 0 || *p == ';' || *p == '#') {
		return 1;
	}

	// parse the name...
	char *name  = p;

	while (*p != 0 && *p != '=') {
		if (isspace(*p)) {
			*p = 0;
		}
		p++;
	}

	if (*p != '=') {
		Die("bad config file, missing '=' on line\n");
	}
	*p++ = 0;

	// parse the value...
	while (*p != 0 && isspace(*p)) {
		p++;
	}

	char *value = p;

	if (*p == '"') {
		// handle a string
		p++;
		value = p;

		while (*p != '"') {
			if (*p == 0) {
				Die("bad config file, unterminated string\n");
			}
			p++;
		}
		*p = 0;

	} else {
		// handle a normal word, whitespace will terminate it
		while (*p != 0) {
			if (isspace(*p) || *p == ';' || *p == '#') {
				*p = 0;
				break;
			}
			p++;
		}

		if (value[0] == 0) {
			Die("bad config file, missing value after '='\n");
		}
	}

	if (! ParseConfigEntry(name, value)) {
		Die("bad config file, unknown variable '%s'\n", name);
	}
	return 1;
}

static void WriteConfigRaw(FILE *fp, const char *name, const char *value, const char *comment) {
	if (comment != NULL) {
		fprintf(fp, "\n");
		fprintf(fp, "; %s\n", comment);
	}
	fprintf(fp, "%s=%s\n", name, value);
}

static void WriteConfigNumber(FILE *fp, const char *name, int value, const char *comment) {
	char buffer[64];
	snprintf(buffer, sizeof(buffer), "%d", value);

	WriteConfigRaw(fp, name, buffer, comment);
}

static void WriteConfigString(FILE *fp, const char *name, const char *value, const char *comment) {
	fprintf(fp, "\n");
	fprintf(fp, "; %s\n", comment);
	fprintf(fp, "%s=\"%s\"\n", name, value);
}

static int WriteDefaultConfigFile(char * filename) {
	FILE * fp = fopen(filename, "w");
	if (fp == NULL) {
		return 0;
	}

	fprintf(fp, ";;\n");
	fprintf(fp, ";; config for Slate\n");
	fprintf(fp, ";;\n");

	char geometry[128];
	snprintf(geometry, sizeof(geometry), "%dx%d", conf.columns, conf.rows);

	WriteConfigString(fp, "font", conf.font, "name of font file (TTF or OTF), \"mono\" or \"regular\" for built-in font");
	WriteConfigNumber(fp, "size", conf.size, "character size of the font");
	WriteConfigRaw   (fp, "geometry", geometry, "columns x rows of the terminal, or the word \"fullscreen\"");
	WriteConfigString(fp, "title", "", "title for the terminal's window");
	WriteConfigString(fp, "shell", "", "default shell to run, plus override for SHELL environment var");
	WriteConfigString(fp, "term",  "", "desired terminal type, plus override for TERM environment var");

	WriteConfigNumber(fp, "backspace", 0, "when > 0, forces backspace key to this character (usually 8 or 127)");
	WriteConfigNumber(fp, "delete",    0, "when > 0, forces delete key to this character (e.g. 127)");
	WriteConfigNumber(fp, "alt",   conf.alt,   "ALT key behavior: 0 = disabled, 1 = ESC prefix, 2 = graphics");
	WriteConfigNumber(fp, "altgr", conf.altgr, "ALTGR key behavior: 0 = same as ALT, 1 = ESC prefix, 2 = graphics");
	WriteConfigNumber(fp, "meta",  conf.meta,  "META key behavior: 0 = disabled, 1 = ESC prefix, 2 = graphics");
	WriteConfigNumber(fp, "gamma", conf.gamma, "gamma of the color palette, 100 = normal, higher is brighter");
	WriteConfigNumber(fp, "delay", conf.delay, "how long (in ms) to delay in main loop when nothing is happening");
	WriteConfigNumber(fp, "bell",  conf.bell,  "bell mode: 0 = disabled, 1..3 = audible (higher is louder), 4..6 = visual");

	WriteConfigNumber(fp, "software", 0, "when > 0, force our window to use a software surface");
	WriteConfigNumber(fp, "hinting", conf.hinting, "level of font hinting: 0 = none, 1 = light, 2 = normal, 3 = mono");
	WriteConfigNumber(fp, "width_frac",   conf.width_frac, "for cell width: w = font_size * width_frac / 100 + width_add");
	WriteConfigNumber(fp, "width_add",    conf.width_add, NULL);
	WriteConfigNumber(fp, "height_frac",  conf.height_frac, "for cell height: h = font_size * height_frac / 100 + height_add");
	WriteConfigNumber(fp, "height_add",   conf.height_add, NULL);
	WriteConfigNumber(fp, "cursor_color", conf.cursor_color, "color for the cursor: entry in the 256-color palette");
	WriteConfigNumber(fp, "max_cache",    conf.max_cache, "maximum amount of memory to use for caching glyphs (in MB)");

	fflush(fp);
	fclose(fp);

	return 1;  // ok
}

static int ReadConfigFile(char * filename) {
	if (filename == NULL) {
		return 0;
	}

	// if it does not exist, create it
	FILE *fp = fopen(filename, "r");
	if (fp == NULL) {
		WriteDefaultConfigFile(filename);
		return 0;
	}

	while (ParseConfigLine(fp)) {
	}

	fclose(fp);
	return 1;  // ok
}

//----------------------------------------------------------------------

int main(int argc, char *argv[]) {
	InitConfig();
	ReadConfigFile(ConfigFileName());
	ParseArguments(argc, argv);
	FinalizeConfig();

	SH_ExecShell();

	C_InitGamma();
	C_InitColors();

	G_LoadFont();

	W_OpenWindow();

	I_InitKeyboard();
	S_InitTerminal();

	for (;;) {
		int act = 0;

		act |= I_PollInput();
		act |= SH_Read();

		W_Redraw();

		if (! act) {
			if (conf.delay > 0) {
				SDL_Delay(conf.delay);
			}
		}

		if (want_quit || SH_HasTerminated()) {
			break;
		}
	}

	W_CloseWindow();
	G_CloseFont();

	SDL_Quit();

	return 0;
}

// show an error message and terminate the program.
void Die(const char *msg, ...) {
	static char buffer[2000];

	va_list arg_ptr;

	va_start(arg_ptr, msg);
	vsnprintf(buffer, sizeof(buffer)-1, msg, arg_ptr);
	va_end(arg_ptr);

	buffer[sizeof(buffer)-1] = 0;

	fprintf(stderr, "\nERROR: %s", buffer);

	W_CloseWindow();
	G_CloseFont();

	SDL_Quit();

	exit(2);
}

//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
