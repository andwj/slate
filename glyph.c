// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

#include "local.h"

#include "SDL_ttf.h"

#define MIN_FONT_SIZE   10
#define MAX_FONT_SIZE   96

// the size of each cell
int cell_w, cell_h;

typedef struct {
	TTF_Font *font;

	SDL_Surface  *cell_surf;
	SDL_Renderer *cell_rend;

	// position in cell for underline and strike-through
	int underline_y;
	int strike_y;

} GlyphState;

static GlyphState win;

//----------------------------------------------------------------------

#define NUM_GLYPH_SLOT  1024

typedef struct CacheNode_s {
	// the character + colors + attributes
	UChar ch;
	uint8_t fg, bg;
	uint16_t attr;

	// the rendered cell
	SDL_Texture * tex;

	// next in hash slot
	struct CacheNode_s * hash_next;
} CacheNode;

typedef struct {
	CacheNode * slots[NUM_GLYPH_SLOT];

	// number of cached single cells
	int total;

	// maximum number of cells
	int maximum;

	// rover for the glyph remover
	int rover;

} GlyphCache;

static GlyphCache cache;

//----------------------------------------------------------------------

void G_InitGlyphCache(void) {
	memset(&cache, 0, sizeof(cache));

	// honor "max_cache" config setting, determine # of cells
	int mb = conf.max_cache;
	if (mb < 1)    { mb = 1;    }
	if (mb > 1000) { mb = 1000; }

	// assume 4 bytes per pixel (RGBA)
	int per_cell = cell_w * cell_h * 4 + (int)sizeof(CacheNode) + 128;

	cache.maximum = (mb * 1024 * 1024) / per_cell;
	if (cache.maximum < 4) {
		cache.maximum = 4;
	}
}

void G_LoadFont(void) {
	// initialize the TTF library
	if (TTF_Init() < 0) {
		Die("cannot initialize TTF: %s\n", SDL_GetError());
	}

	int size = conf.size;

	if (size < MIN_FONT_SIZE) size = MIN_FONT_SIZE;
	if (size > MAX_FONT_SIZE) size = MAX_FONT_SIZE;

	int embedded = 0;  // 1 or 2

	if (conf.font == NULL) {
		Die("failed to load font: missing name\n");
	} if (UT_Compare(conf.font, "mono") == 0) {
		embedded = 1;
	} else if (UT_Compare(conf.font, "regular") == 0) {
		embedded = 2;
	}

	if (embedded > 0) {
#ifdef EMBEDDED_FONTS
		const EmbeddedFont * info = &font_Go_Mono;
		if (embedded == 2) {
			 info = &font_Go_Regular;
		}

		SDL_RWops * rw = SDL_RWFromConstMem(info->data, info->size);

		win.font = TTF_OpenFontRW(rw, 1, size);
#else
		Die("no embedded font, please specify a full filename\n");
#endif
	} else {
		win.font = TTF_OpenFont(conf.font, size);
	}

	if (win.font == NULL) {
		Die("failed to load font: %s\n", SDL_GetError());
	}

	// set style and misc stuff
	TTF_SetFontKerning(win.font, 1);

	int hinting = TTF_HINTING_NORMAL;
	switch (conf.hinting) {
	case 0: hinting = TTF_HINTING_NONE;  break;
	case 1: hinting = TTF_HINTING_LIGHT; break;
	case 3: hinting = TTF_HINTING_MONO;  break;
	default: break;
	}

	TTF_SetFontHinting(win.font, hinting);

	/* determine cell size */

	// the following heuristic seems to work fairly well.
	// default width_frac is around 65%, height_frac is around 120%).
	//
	// I tried using the font metrics, but the results were actually worse,
	// especially the LineSkip height can be much bigger than necessary.

	cell_w = size * conf.width_frac  / 100 + conf.width_add;
	cell_h = size * conf.height_frac / 100 + conf.height_add;

/*
	int minx = -1, maxx = -1, miny = -1, maxy = -1, advance = -1;
	if (W_FontHasChar('N')) {
		TTF_GlyphMetrics(win.font, 'N', &minx, &maxx, &miny, &maxy, &advance);
	} else {
		TTF_GlyphMetrics(win.font, '?', &minx, &maxx, &miny, &maxy, &advance);
	}
*/

	// clamp values to something sane-ish
	if (cell_w < MIN_FONT_SIZE / 2) cell_w = MIN_FONT_SIZE / 2;
	if (cell_h < MIN_FONT_SIZE    ) cell_h = MIN_FONT_SIZE;

	if (cell_w > MAX_FONT_SIZE * 2) cell_w = MAX_FONT_SIZE * 2;
	if (cell_h > MAX_FONT_SIZE * 2) cell_h = MAX_FONT_SIZE * 2;

	win.cell_surf = SDL_CreateRGBSurface(SDL_SWSURFACE, cell_w, cell_h, 32,
		0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
	if (win.cell_surf == NULL) {
		Die("cannot create cell_surf: %s\n", SDL_GetError());
	}

	win.cell_rend = SDL_CreateSoftwareRenderer(win.cell_surf);
	if (win.cell_rend == NULL) {
		Die("cannot create cell_rend: %s\n", SDL_GetError());
	}

	// compute position of underline and strike-through lines
	int ascent  = TTF_FontAscent(win.font);
//	int descent = TTF_FontDescent(win.font);

	win.underline_y = ascent + (size / 10) + 1;
	if (win.underline_y > cell_h - 1) {
		win.underline_y = cell_h - 1;
	}
	win.strike_y = ascent * 2 / 3;

	G_InitGlyphCache();
}

void G_CloseFont(void) {
	if (win.font != NULL) {
		TTF_CloseFont(win.font);
		win.font = NULL;
	}

	TTF_Quit();
}

int G_FontHasChar(UChar ch) {
	// SDL_TTF only supports 16-bit Unicode chars
	if (ch > 0xFFFF) {
		return 0;
	}
	if (TTF_GlyphIsProvided(win.font, ch)) {
		return 1;
	}
	return 0;
}

void G_UncacheGlyph(void) {
	for (;;) {
		cache.rover = (cache.rover + 1) % NUM_GLYPH_SLOT;

		CacheNode *node = cache.slots[cache.rover];

		if (node != NULL) {
			cache.total -= 1;

			// remove the last element (since it is oldest)
			if (node->hash_next == NULL) {
				cache.slots[cache.rover] = NULL;
			} else {
				CacheNode *prev = node;

				while (prev->hash_next->hash_next != NULL) {
					prev = prev->hash_next;
				}
				node = prev->hash_next;
				prev->hash_next = NULL;
			}

			SDL_DestroyTexture(node->tex);

			node->tex = NULL;
			node->hash_next = NULL;

			free(node);
			return;
		}
	}
}

CacheNode * G_SearchCache(UChar ch, uint8_t fg, uint8_t bg, uint16_t attr) {
	// if not found, creates the CacheNode and returns it with tex == NULL.

	// compute a hash
	uint32_t x1 = (ch & 0xFFF);
	uint32_t x2 = (ch >> 12);

	uint32_t hash = 0;

	hash = hash ^ (x1 * 229);
	hash = hash ^ (x2 * 383);

	hash = hash ^ ((uint32_t) fg   * 461);
	hash = hash ^ ((uint32_t) bg   * 577);
	hash = hash ^ ((uint32_t) attr * 643);

	hash = hash ^ (hash / NUM_GLYPH_SLOT / NUM_GLYPH_SLOT);
	hash = hash ^ (hash / NUM_GLYPH_SLOT);
	hash = hash % NUM_GLYPH_SLOT;

	CacheNode *node = cache.slots[hash];

	while (node != NULL) {
		if (node->ch == ch && node->fg == fg &&
			node->bg == bg && node->attr == attr) {

			// found it!
			return node;
		}

		node = node->hash_next;
	}

	// not found, create a node and link it in.
	// when cache is full, remove a glyph at random.

	if (cache.total >= cache.maximum) {
		G_UncacheGlyph();
	}

	node = (CacheNode *) UT_Alloc(sizeof(CacheNode));

	node->ch   = ch;
	node->fg   = fg;
	node->bg   = bg;
	node->attr = attr;

	node->tex = NULL;

	node->hash_next = cache.slots[hash];
	cache.slots[hash] = node;
	cache.total += 1;

	return node;
}

SDL_Texture * G_RenderCell(SDL_Renderer *R, UChar ch, uint8_t c_fg, uint8_t c_bg, uint16_t c_attr) {
	// use the super clever caching stuff
	CacheNode * node = G_SearchCache(ch, c_fg, c_bg, c_attr);

	if (node->tex != NULL) {
		return node->tex;
	}

	SDL_Color bg, fg;

	C_GetColor(c_bg, &bg);
	C_GetColor(c_fg, &fg);

	// guarantee a minimal contrast between fg and bg.
	C_Contrast(&bg, &fg);

	SDL_SetRenderDrawColor(win.cell_rend, bg.r, bg.g, bg.b, 0xFF);
	SDL_RenderClear(win.cell_rend);

	if (ch == 0xFFFD) {
		SDL_SetRenderDrawColor(win.cell_rend, fg.r, fg.g, fg.b, 0xFF);
		B_RenderDiamond(win.cell_rend, cell_w, cell_h);

		SDL_SetRenderDrawColor(win.cell_rend, bg.r, bg.g, bg.b, 0xFF);
		B_RenderQuestionMark(win.cell_rend, cell_w, cell_h);

	} else if (B_IsBlockChar(ch)) {
		SDL_SetRenderDrawColor(win.cell_rend, fg.r, fg.g, fg.b, 0xFF);
		B_RenderBlockChar(win.cell_rend, ch, cell_w, cell_h);

	} else {
		int style = 0;

		if (c_attr & ATTR_BOLD)   { style |= TTF_STYLE_BOLD;   }
		if (c_attr & ATTR_ITALIC) { style |= TTF_STYLE_ITALIC; }

		TTF_SetFontStyle(win.font, style);

		// determine width of the character
		int minx = -1, maxx = -1, miny = -1, maxy = -1, advance = -1;

		TTF_GlyphMetrics(win.font, ch, &minx, &maxx, &miny, &maxy, &advance);

		if (maxx < 1) {
			minx = 0;
			maxx = cell_w;
		}

		int width = maxx - minx;

		SDL_Surface *
		surf = TTF_RenderGlyph_Shaded(win.font, ch, fg, bg);
		if (surf == NULL) {
			Die("cannot render glyph U+%04x: %s\n", ch, SDL_GetError());
		}

		SDL_Texture *
		surfTex = SDL_CreateTextureFromSurface(win.cell_rend, surf);
		if (surfTex == NULL) {
			Die("cannot create tex: %s\n", SDL_GetError());
		}

		SDL_Rect pos;

		// for proportional fonts, ensure char is horizontally centered
		pos.x = (cell_w - width) / 2 - minx;
		pos.y = 0;
		pos.w = surf->w;
		pos.h = surf->h;

		SDL_RenderCopy(win.cell_rend, surfTex, NULL, &pos);

		SDL_DestroyTexture(surfTex);
		surfTex = NULL;

		SDL_FreeSurface(surf);
		surf = NULL;

		// handle underlining and strike-through
		if (c_attr & ATTR_UNDERLINE) {
			SDL_SetRenderDrawColor(win.cell_rend, fg.r, fg.g, fg.b, 0xFF);
			B_RenderHorizLine(win.cell_rend, win.underline_y, cell_w, cell_h);
		}
		if (c_attr & ATTR_STRIKE) {
			SDL_SetRenderDrawColor(win.cell_rend, fg.r, fg.g, fg.b, 0xFF);
			B_RenderHorizLine(win.cell_rend, win.strike_y, cell_w, cell_h);
		}
	}

	SDL_Texture *
	tex = SDL_CreateTextureFromSurface(R, win.cell_surf);
	if (tex == NULL) {
		Die("cannot creating tex: %s\n", SDL_GetError());
	}

	node->tex = tex;

	return tex;
}
