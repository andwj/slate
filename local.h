// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

#ifndef __SLATE_LOCALDEFS_H__
#define __SLATE_LOCALDEFS_H__

/* Windows support */

#if defined(_WIN32) || defined(_WIN64)
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#ifndef __WIN32__
#define __WIN32__
#endif
#endif

/* libc includes */

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

/* LibSDL v2 */

#include "SDL.h"

/* utility macros */

#ifndef MAX
#define MAX(a,b)  ((a) > (b) ? (a) : (b))
#endif

#ifndef MIN
#define MIN(a,b)  ((a) < (b) ? (a) : (b))
#endif

#ifndef ABS
#define ABS(a)  ((a) < 0 ? -(a) : (a))
#endif

/* common defs */

// a unicode code-point
typedef uint32_t UChar;

// attributes of a character cell
typedef enum {
	ATTR_NONE        = 0,

	ATTR_BOLD        = (1 << 0),
	ATTR_DIM         = (1 << 1),
	ATTR_ITALIC      = (1 << 2),
	ATTR_UNDERLINE   = (1 << 3),
	ATTR_REVERSE     = (1 << 4),
	ATTR_STRIKE      = (1 << 5),
	ATTR_HIDDEN      = (1 << 6),

	ATTR_IS_WIDE     = (1 << 12),
	ATTR_IS_SELECTED = (1 << 13),
	ATTR_IS_DIRTY    = (1 << 14),
} attribute_enum;

/* audio.c */

void A_AudibleBell(SDL_Window *window);

/* blocks.c */

int  B_IsBlockChar(UChar ch);
void B_RenderBlockChar(SDL_Renderer *R, UChar ch, int w, int h);
void B_RenderDiamond(SDL_Renderer *R, int w, int h);
void B_RenderQuestionMark(SDL_Renderer *R, int w, int h);
void B_RenderHorizLine(SDL_Renderer *R, int y, int w, int h);

/* colors.c */

extern uint8_t gamma[256];
extern uint8_t palette[256][3];

void C_InitGamma(void);
void C_InitColors(void);
void C_GetColor(uint8_t col, SDL_Color *out);
void C_Contrast(SDL_Color *bg, SDL_Color *fg);

uint8_t C_DimColor(uint8_t c);
uint8_t C_OppositeColor(int c);
uint8_t C_ClosestRGB(int r, int g, int b);

/* draw.c */

#define MIN_WIDTH   8
#define MIN_HEIGHT  8

#define MAX_WIDTH   255
#define MAX_HEIGHT  511

void W_OpenWindow(void);
void W_CloseWindow(void);
void W_Redraw(void);
void W_Focus(int focus);

void S_InitTerminal(void);
void S_DirtyAll(void);
void S_ProcessChar(UChar ch);
void S_ScrollBack(int delta);
void S_WindowResize(int pixel_w, int pixel_h);

/* font_nn.c */

typedef struct {
	int   size;
	const uint8_t * data;
} EmbeddedFont;

#ifdef EMBEDDED_FONTS
extern const EmbeddedFont font_Go_Mono;
extern const EmbeddedFont font_Go_Regular;
#endif

/* glyph.c */

extern int cell_w, cell_h;

void G_LoadFont(void);
void G_CloseFont(void);
int  G_FontHasChar(UChar ch);

SDL_Texture * G_RenderCell(SDL_Renderer *R, UChar ch, uint8_t c_fg, uint8_t c_bg, uint16_t c_attr);

/* input.c */

void I_InitKeyboard(void);
void I_TerminalID(void);
void I_KeypadMode(int appl_mode);
void I_CursorMode(int appl_mode);
void I_WaitAnyKey(void);
int  I_PollInput(void);

/* shell.c */

void SH_SpecifyCommand(int argc, char *argv[]);
void SH_ExecShell(void);
int  SH_HasTerminated(void);
void SH_SetWindowSize(int w, int h);
void SH_SendBreak(void);

int  SH_Read(void);
void SH_Write(UChar ch);
void SH_WriteStr(const char *s);

/* tables.c */

#define U_GRAVE       0x0300
#define U_ACUTE       0x0301
#define U_CIRCUMFLEX  0x0302
#define U_TILDE       0x0303
#define U_UMLAUT      0x0308

#define U_MACRON      0x0304
#define U_BREVE       0x0306
#define U_OVERDOT     0x0307
#define U_RING        0x030A
#define U_HACEK       0x030C

UChar T_CombineChars(UChar c1, UChar c2);
UChar T_FallbackChar(UChar ch);

int T_IsSpace(UChar ch);
int T_IsPrint(UChar ch);
int T_IsMark (UChar ch);

/* util.c */

void * UT_Alloc(size_t size);
int    UT_MakeDir(const char *name);
int    UT_Compare(const char *A, const char *B);
char * UT_Strdup(const char *s);
char * UT_JoinPath(const char *dir, const char *rest);
int    UT_MatchArg(const char *arg, char shortname, const char *longname);
int    UT_HasBOM(const char *s);

/* main.c */

typedef struct Config_s {
	// set if the window should occupy the whole screen and have no
	// borders or other decoration.
	int    fullscreen;

	// the requested geometry (number of columns and rows of characters).
	// not used when "fullscreen" above is set.
	int    columns, rows;

	// name of font file to use (a TTF or OTF font).
	// can be "mono" or "regular" to use the embedded fonts.
	char * font;

	// size of font (the point size).
	// character cells will be larger than this.
	int    size;

	// title for the window.
	// if unset, a default title will be chosen.
	char * title;

	// the default shell program to run (when not explicitly given on
	// the command line).  plus it overrides the SHELL environment var.
	char * shell;

	// the terminal type that the user wishes use to emulate.
	// plus it overrides the TERM environment var given to the shell.
	char * term;

	// when > 0, overrides the character send by BACKSPACE key
	int    backspace;

	// when > 0, overrides the character send by DELETE key
	int    delete;

	// ALT key behavior: 0 = disabled, 1 = ESC prefix, 2 = graphics
	int    alt;

	// ALTGR key behavior: 0 = same as ALT, 1 = ESC prefix, 2 = graphics
	int    altgr;

	// META key behavior: 0 = disabled, 1 = ESC prefix, 2 = graphics
	int    meta;

	// 0 = US layout, 1 = UK (British) layout
	int    keyboard;

	// gamma level for color palette.  100 = normal, higher is brighter
	int    gamma;

	// how long (in ms) to delay in main loop when nothing is happening
	int    delay;

	// bell mode: 0 = disabled, 1..3 = audible (higher is louder), 4..6 = visual
	int    bell;

	// when > 0, force our window to use a software surface
	int    software;

	// level of font hinting: 0 = none, 1 = light, 2 = normal, 3 = mono
	int    hinting;

	// for cell width: w = font_size * width_frac / 100 + width_add
	int    width_frac;
	int    width_add;

	// for cell height: h = font_size * height_frac / 100 + height_add
	int    height_frac;
	int    height_add;

	// color for the cursor: entry in the 256-color palette
	int    cursor_color;

	// maximum amount of memory to use for caching glyphs (in MB)
	int    max_cache;

} Config;

extern Config conf;
extern int want_quit;

void Die(const char *msg, ...);

#endif /* __SLATE_LOCALDEFS_H__ */
