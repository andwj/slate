#
#  --- SLATE ---
#
#  Makefile for Linux and BSD systems.
#  Requires GNU make.
#

PROGRAM=slate

# CC=clang-11

BASEFLAG=-std=c99
OPTIMISE=-O0 -g3
WARNINGS=-Wall -Wextra -Wno-unused-parameter

CFLAGS ?= $(BASEFLAG) $(OPTIMISE) $(WARNINGS)
CPPFLAGS ?=
LDFLAGS ?= $(OPTIMISE)
LIBS ?=

LIBS += -lm
LIBS += -lutil   # for openpty()

OBJ_DIR=_build

DUMMY=$(OBJ_DIR)/__dummy


#----- Libraries ----------------------------------------------

# comment the next two lines to disable the use of dlsym
# (which is only needed to get address of XBell function).
CPPFLAGS += -DXBELL_DYNAMIC
LIBS     += -ldl

# comment the next line to not embed any fonts into the final
# executable.  you can remove "font_1.o" and "font_2.o" objects
# from the build too (but it is not required).
CPPFLAGS += -DEMBEDDED_FONTS

# stuff for LibSDL 2
CPPFLAGS += -I/usr/include/SDL2
LIBS     += -lSDL2 -lSDL2_ttf


#----- Object files ----------------------------------------------

OBJS = \
	$(OBJ_DIR)/audio.o   \
	$(OBJ_DIR)/block.o   \
	$(OBJ_DIR)/colors.o  \
	$(OBJ_DIR)/draw.o    \
	$(OBJ_DIR)/font_1.o  \
	$(OBJ_DIR)/font_2.o  \
	$(OBJ_DIR)/glyph.o   \
	$(OBJ_DIR)/input.o   \
	$(OBJ_DIR)/main.o    \
	$(OBJ_DIR)/shell.o   \
	$(OBJ_DIR)/tables.o  \
	$(OBJ_DIR)/util.o

$(OBJ_DIR)/%.o: ./%.c
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ -c $<


#----- Targets -----------------------------------------------

all: $(DUMMY) $(PROGRAM)

clean:
	rm -f $(PROGRAM) $(OBJ_DIR)/*.[oa] $(OBJ_DIR)/font_to_c
	rm -f font_1.c font_2.c core ERRS

$(PROGRAM): $(OBJS)
	$(CC) $^ -o $@ $(LDFLAGS) $(LIBS)

# this is used to create the OBJ_DIR directory
$(DUMMY):
	mkdir -p $(OBJ_DIR)
	@touch $@

.PHONY: all clean


#----- Embedded Fonts ----------------------------------------------

./font_1.c: $(OBJ_DIR)/font_to_c fonts/Go-Mono.ttf
	$(OBJ_DIR)/font_to_c fonts/Go-Mono.ttf > $@

./font_2.c: $(OBJ_DIR)/font_to_c fonts/Go-Regular.ttf
	$(OBJ_DIR)/font_to_c fonts/Go-Regular.ttf > $@

$(OBJ_DIR)/font_to_c: misc/font_to_c.c
	$(CC) $(CFLAGS) $< -o $@


#--- editor settings ------------
# vi:ts=8:sw=8:noexpandtab
